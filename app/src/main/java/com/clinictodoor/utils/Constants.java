package com.clinictodoor.utils;

/**
 * Created by lenovo on 6/2/2018.
 */

public class Constants {

    public static final String BaseUrl = "http://c2dapi.workr.in/1.0/";
    public static final String ImageBaseUrl = "https://s3.ap-south-1.amazonaws.com/clinic2door/profile/";
    public static final String DefaultImageUrl = "default.png";

    public static final String SendOtpUrl = "send-otp";
    public static final String SignUpUrl = "signup";
    public static final String LoginUrl = "login";
    public static final String SaveProfileUrl = "user/profile";
    public static final String GetAddressUrl = "user/address";
    public static final String GetFamilyMembersUrl = "user/family-member";
    public static final String GetServicesUrl = "services";
    public static final String UpdateProfilePicUrl = "user/pic";
    public static final String GetCitiesUrl = "cities";
    public static final String GetCounteryUrl = "countries";
    public static final String AvailableSlotsUrl = "service/available-slots";
    public static final String GetPackagesUrl = "packages";
    public static final String GetTestsUrl = "tests";
}
