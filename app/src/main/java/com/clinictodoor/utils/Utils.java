package com.clinictodoor.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;

import java.util.Calendar;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by lenovo on 6/1/2018.
 */

public class Utils {

    public static Typeface mTypeface(Context ctx, int typeface) {
        Typeface font1 = null;
        if (typeface == 1) {
            font1 = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans_ExtraBold.ttf");
        } else if (typeface == 2) {
            font1 = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans_Light.ttf");
        } else if (typeface == 3) {
            font1 = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans_Regular.ttf");
        } else if (typeface == 4) {
            font1 = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans_Semibold.ttf");
        }
        return font1;
    }


    public static String mDateConversion(String month) {
        String mon = "";
        if (month.equals("01")) {
            mon = "JAN";
        } else if (month.equals("02")) {
            mon = "FEB";
        } else if (month.equals("03")) {
            mon = "MAR";
        } else if (month.equals("04")) {
            mon = "APR";
        } else if (month.equals("05")) {
            mon = "MAY";
        } else if (month.equals("06")) {
            mon = "JUNE";
        } else if (month.equals("07")) {
            mon = "JLY";
        } else if (month.equals("08")) {
            mon = "AUG";
        } else if (month.equals("09")) {
            mon = "SEP";
        } else if (month.equals("10")) {
            mon = "OCT";
        } else if (month.equals("11")) {
            mon = "NOV";
        } else if (month.equals("12")) {
            mon = "DEC";
        }
        return mon;
    }


    private static final int PERMISSION_REQUEST_CODE = 200;

    public static boolean checkPermission(Context ctx) {
        int result1 = ContextCompat.checkSelfPermission(ctx, WRITE_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(ctx, READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(ctx, ACCESS_FINE_LOCATION);
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    public static String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public static boolean isGPSEnabled(Context mContext) {
        LocationManager lm = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static void customToast(Context ctx, String message) {
        Toast ToastMessage = Toast.makeText(ctx, message, Toast.LENGTH_SHORT);
        View toast_view = ToastMessage.getView();
        toast_view.setBackgroundResource(R.drawable.toast_background_color);
        TextView toast_text = toast_view.findViewById(android.R.id.message);
        toast_text.setTextColor(ctx.getResources().getColor(R.color.white_color));
        toast_text.setPadding(20, 0, 20, 0);
        toast_text.setTypeface(Utils.mTypeface(ctx, 3));
        ToastMessage.show();
    }

}
