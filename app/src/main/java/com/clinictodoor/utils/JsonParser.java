package com.clinictodoor.utils;

import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CountriesDto;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.dtos.UserDetailsDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 6/2/2018.
 */

public class JsonParser {


    public UserDetailsDto mUserDetails(String response) throws JSONException {
        UserDetailsDto dto = new UserDetailsDto();
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("userDetails")) {
            JSONObject json = jobj.getJSONObject("userDetails");
            if (jobj.has("id")) {
                dto.setId(json.getString("id"));
            }
            if (json.has("username")) {
                dto.setUsername(json.getString("username"));
            }
            if (json.has("account_verified")) {
                dto.setAccount_verified(json.getString("account_verified"));
            }
            if (json.has("user_type")) {
                dto.setUser_type(json.getString("user_type"));
            }
            if (json.has("created_at")) {
                dto.setCreated_at(json.getString("created_at"));
            }
            if (json.has("updated_at")) {
                dto.setUpdated_at(json.getString("updated_at"));
            }
            if (json.has("profile")) {
                if (!json.getString("profile").equals("null")) {
                    JSONObject j_profile = json.getJSONObject("profile");
                    dto.setProfileDto(mProfileDetails(j_profile));
                }
            }
        }
        return dto;
    }

    private ProfileDto mProfileDetails(JSONObject j_profile) throws JSONException {
        ProfileDto dto_profile = new ProfileDto();
        if (j_profile.has("id")) {
            dto_profile.setId(j_profile.getString("id"));
        }
        if (j_profile.has("salutation")) {
            dto_profile.setSalutation(j_profile.getString("salutation"));
        }
        if (j_profile.has("first_name")) {
            dto_profile.setFirst_name(j_profile.getString("first_name"));
        }
        if (j_profile.has("last_name")) {
            dto_profile.setLast_name(j_profile.getString("last_name"));
        }
        if (j_profile.has("gender")) {
            dto_profile.setGender(j_profile.getString("gender"));
        }
        if (j_profile.has("dob")) {
            dto_profile.setDob(j_profile.getString("dob"));
        }
        if (j_profile.has("user_id")) {
            dto_profile.setUser_id(j_profile.getString("user_id"));
        }
        return dto_profile;
    }


    public ProfileDto mUserProfile(String response) throws JSONException {
        ProfileDto dto = new ProfileDto();
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("user_profile")) {
            JSONObject json = jobj.getJSONObject("user_profile");
            if (json.has("emr_no")) {
                dto.setEmr_no(json.getString("emr_no"));
            }
            if (json.has("username")) {
                dto.setUsername(json.getString("username"));
            }
            if (json.has("profile")) {
                if (!json.getString("profile").equals("null")) {
                    JSONObject jobj_new = json.getJSONObject("profile");
                    if (jobj_new.has("id")) {
                        dto.setId(jobj_new.getString("id"));
                    }
                    if (jobj_new.has("salutation")) {
                        dto.setSalutation(jobj_new.getString("salutation"));
                    }
                    if (jobj_new.has("first_name")) {
                        dto.setFirst_name(jobj_new.getString("first_name"));
                    }
                    if (jobj_new.has("last_name")) {
                        dto.setLast_name(jobj_new.getString("last_name"));
                    }
                    if (jobj_new.has("gender")) {
                        dto.setGender(jobj_new.getString("gender"));
                    }
                    if (jobj_new.has("dob")) {
                        dto.setDob(jobj_new.getString("dob"));
                    }
                    if (jobj_new.has("user_id")) {
                        dto.setUser_id(jobj_new.getString("user_id"));
                    }
                    if (jobj_new.has("email")) {
                        dto.setEmail(jobj_new.getString("email"));
                    }
                    if (jobj_new.has("pic")) {
                        dto.setPic(jobj_new.getString("pic"));
                    }
                } else {

                }
            }
        }
        return dto;
    }

    List<AddressDto> list_address = new ArrayList<>();

    public List<AddressDto> mAddressList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("user_addresses")) {
            JSONArray jarr = jobj.getJSONArray("user_addresses");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_address.add(mAddressDetails(json));
            }
        }
        return list_address;
    }

    private AddressDto mAddressDetails(JSONObject json) throws JSONException {
        AddressDto dto = new AddressDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("address_type")) {
            dto.setAddress_type(json.getString("address_type"));
        }
        if (json.has("door_no")) {
            dto.setDoor_no(json.getString("door_no"));
        }
        if (json.has("street")) {
            dto.setStreet(json.getString("street"));
        }
        if (json.has("landmark")) {
            dto.setLandmark(json.getString("landmark"));
        }
        if (json.has("city")) {
            dto.setCity(json.getString("city"));
        }
        if (json.has("state")) {
            dto.setState(json.getString("state"));
        }
        if (json.has("country")) {
            dto.setCountry(json.getString("country"));
        }
        if (json.has("pin_code")) {
            dto.setPin_code(json.getString("pin_code"));
        }
        return dto;
    }

    List<FamilyMembersDto> list_familyMemb = new ArrayList<>();

    public List<FamilyMembersDto> mFamilyMembList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("family_members")) {
            JSONArray jarr = jobj.getJSONArray("family_members");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_familyMemb.add(mFamilyDetails(json));
            }
        }
        return list_familyMemb;
    }

    private FamilyMembersDto mFamilyDetails(JSONObject json) throws JSONException {
        FamilyMembersDto dto = new FamilyMembersDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("relationship")) {
            dto.setRelationship(json.getString("relationship"));
        }
        if (json.has("salutation")) {
            dto.setSalutation(json.getString("salutation"));
        }
        if (json.has("first_name")) {
            dto.setFirst_name(json.getString("first_name"));
        }
        if (json.has("last_name")) {
            dto.setLast_name(json.getString("last_name"));
        }
        if (json.has("gender")) {
            dto.setGender(json.getString("gender"));
        }
        if (json.has("dob")) {
            dto.setDob(json.getString("dob"));
        }
        if (json.has("mobile")) {
            dto.setMobile(json.getString("mobile"));
        }
        if (json.has("email")) {
            dto.setEmail(json.getString("email"));
        }
        if (json.has("door_no")) {
            dto.setDoor_no(json.getString("door_no"));
        }
        if (json.has("street")) {
            dto.setStreet(json.getString("street"));
        }
        if (json.has("landmark")) {
            dto.setLandmark(json.getString("landmark"));
        }
        if (json.has("city")) {
            dto.setCity(json.getString("city"));
        }
        if (json.has("state")) {
            dto.setState(json.getString("state"));
        }
        if (json.has("country")) {
            dto.setCountry(json.getString("country"));
        }
        if (json.has("pin_code")) {
            dto.setPin_code(json.getString("pin_code"));
        }
        return dto;
    }

    List<ServicesDto> list_services = new ArrayList<>();

    public List<ServicesDto> mServicesList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("services")) {
            JSONArray jarr = jobj.getJSONArray("services");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_services.add(mServiceDetails(json));
            }
        }
        return list_services;
    }

    private ServicesDto mServiceDetails(JSONObject json) throws JSONException {
        ServicesDto dto = new ServicesDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("title")) {
            dto.setTitle(json.getString("title"));
        }
        if (json.has("code")) {
            dto.setCode(json.getString("code"));
        }
        if (json.has("has_fixed_price")) {
            dto.setHas_fixed_price(json.getString("has_fixed_price"));
        }
        if (json.has("price")) {
            dto.setPrice(json.getString("price"));
        }
        if (json.has("description")) {
            dto.setDescription(json.getString("description"));
        }
        if (json.has("has_multiple_packages")) {
            dto.setHas_multiple_packages(json.getString("has_multiple_packages"));
        }
        if (json.has("has_multiple_tests")) {
            dto.setHas_multiple_tests(json.getString("has_multiple_tests"));
        }
        return dto;
    }

    List<CountriesDto> list_countries = new ArrayList<>();

    public List<CountriesDto> mCountryCodeList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("countries")) {
            JSONArray jarr = jobj.getJSONArray("countries");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_countries.add(mCountryCodeDetails(json));
            }
        }
        return list_countries;
    }

    private CountriesDto mCountryCodeDetails(JSONObject json) throws JSONException {
        CountriesDto dto = new CountriesDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("name")) {
            dto.setName(json.getString("name"));
        }
        if (json.has("country_code")) {
            dto.setCountry_code(json.getString("country_code"));
        }
        if (json.has("calling_code")) {
            dto.setCalling_code(json.getString("calling_code"));
        }
        return dto;
    }

    List<PackagesDto> list_packages = new ArrayList<>();

    public List<PackagesDto> mPackagesList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("service_packages")) {
            JSONArray jarr = jobj.getJSONArray("service_packages");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_packages.add(mPackageDetails(json));
            }
        }
        return list_packages;
    }

    private PackagesDto mPackageDetails(JSONObject json) throws JSONException {
        PackagesDto dto = new PackagesDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("title")) {
            dto.setTitle(json.getString("title"));
        }
        if (json.has("price")) {
            dto.setPrice(json.getString("price"));
        }
        if (json.has("description")) {
            dto.setDescription(json.getString("description"));
        }
        if (json.has("pivot")) {
            JSONObject jobj = json.getJSONObject("pivot");
            if (jobj.has("service_id")) {
                dto.setService_id(jobj.getString("service_id"));
            }
        }
        return dto;
    }

    List<PackagesDto> list_tests = new ArrayList<>();

    public List<PackagesDto> mTestsList(String response) throws JSONException {
        JSONObject jobj = new JSONObject(response);
        if (jobj.has("service_tests")) {
            JSONArray jarr = jobj.getJSONArray("service_tests");
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject json = jarr.getJSONObject(i);
                list_tests.add(mTestDetails(json));
            }
        }
        return list_tests;
    }

    private PackagesDto mTestDetails(JSONObject json) throws JSONException {
        PackagesDto dto = new PackagesDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("title")) {
            dto.setTitle(json.getString("title"));
        }
        if (json.has("price")) {
            dto.setPrice(json.getString("price"));
        }
        if (json.has("pivot")) {
            JSONObject jobj = json.getJSONObject("pivot");
            if (jobj.has("service_id")) {
                dto.setService_id(jobj.getString("service_id"));
            }
        }
        return dto;
    }
}
