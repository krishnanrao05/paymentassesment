package com.clinictodoor.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateAddressActivity;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.utils.Utils;

import java.util.List;

/**
 * Created by lenovo on 6/10/2018.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    Context ctx;
    List<AddressDto> list_address;

    public AddressAdapter(Context ctx, List<AddressDto> list_address) {
        this.ctx = ctx;
        this.list_address = list_address;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AddressDto address_dto = list_address.get(position);
        holder.addressType.setText(address_dto.getAddress_type());
        holder.tv_address.setText(address_dto.getDoor_no() + "," + address_dto.getStreet() + "\n" + address_dto.getLandmark() + "," + address_dto.getCity() + "\n" + address_dto.getState() + "," + address_dto.getCountry() + "-" + address_dto.getPin_code());


        holder.addressType.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_address.setTypeface(Utils.mTypeface(ctx, 3));
//        holder.tv_deleteAddress.setTypeface(Utils.mTypeface(ctx, 4));
//        holder.tv_editAddress.setTypeface(Utils.mTypeface(ctx, 4));
    }

    @Override
    public int getItemCount() {
        return list_address.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView addressType, tv_address;
        ImageView img_edit, img_delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            addressType = itemView.findViewById(R.id.addressType);
            tv_address = itemView.findViewById(R.id.tv_address);
            img_edit = itemView.findViewById(R.id.img_edit);
            img_delete = itemView.findViewById(R.id.img_delete);

            img_edit.setOnClickListener(this);
            img_delete.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_edit:
                    Intent in_updateAdd = new Intent(ctx, CreateAddressActivity.class);
                    in_updateAdd.putExtra("address_dto", list_address.get(getPosition()));
                    ctx.startActivity(in_updateAdd);
                    break;
                case R.id.img_delete:

                    break;
            }
        }
    }
}
