package com.clinictodoor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;
import com.clinictodoor.activities.CartActivity;
import com.clinictodoor.database.DBHelper;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyCardHolder> {

    Context ctx;
    List<CartDto> list_cart;
    List<AddressDto> list_address;
    List<FamilyMembersDto> list_family;
    DBHelper db;


    public CartAdapter(Context ctx, List<CartDto> list_cart, List<FamilyMembersDto> list_family, List<AddressDto> list_address) {
        this.ctx = ctx;
        this.list_cart = list_cart;
        this.list_address = list_address;
        this.list_family = list_family;
        db = new DBHelper(ctx);
    }

    public void updateReceiptsList(List<CartDto> newlist) {
        list_cart.clear();
        list_cart.addAll(newlist);
        this.notifyDataSetChanged();
    }

    @Override
    public MyCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_cart = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
        return new MyCardHolder(view_cart);
    }

    @Override
    public void onBindViewHolder(MyCardHolder holder, int position) {

        holder.tv_price.setText(ctx.getResources().getString(R.string.Price) + " " + list_cart.get(position).getService_image());
        holder.tv_cartItemName.setText(list_cart.get(position).getService_name());
        holder.tv_date.setText(list_cart.get(position).getService_date());
        holder.tv_time.setText(list_cart.get(position).getService_time());
        holder.tv_relation.setText(list_cart.get(position).getFamily_name());
        holder.tv_address.setText(list_cart.get(position).getAddress_name());

    }

    @Override
    public int getItemCount() {
        return list_cart.size();
    }

    public class MyCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_cartItemName, tv_price, tv_relation, tv_address, tv_date, tv_time;
        RelativeLayout rel_family, rel_address, rel_date;
        ImageView img_delete;

        public MyCardHolder(View itemView) {
            super(itemView);

            tv_cartItemName = itemView.findViewById(R.id.tv_cartItemName);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_relation = itemView.findViewById(R.id.tv_relation);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            rel_family = itemView.findViewById(R.id.rel_family);
            rel_address = itemView.findViewById(R.id.rel_address);
            rel_date = itemView.findViewById(R.id.rel_date);
            img_delete = itemView.findViewById(R.id.img_delete);

            tv_cartItemName.setTypeface(Utils.mTypeface(ctx, 4));
            tv_price.setTypeface(Utils.mTypeface(ctx, 4));
            tv_relation.setTypeface(Utils.mTypeface(ctx, 3));
            tv_address.setTypeface(Utils.mTypeface(ctx, 3));
            tv_date.setTypeface(Utils.mTypeface(ctx, 3));
            tv_time.setTypeface(Utils.mTypeface(ctx, 3));

            img_delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_delete:
                    long value = db.deleteCartItem(list_cart.get(getPosition()).getId());
                    if (value == 1) {
                        Utils.customToast(ctx, "Cart Item removed successfully.");
                        List<CartDto> list = db.getCartList();
                        updateReceiptsList(list);
                        ((CartActivity) ctx).notifyCartPrice();
                    } else {
                        Utils.customToast(ctx, "Failed to delete the Cart Item.");
                    }
//                    Toast.makeText(ctx, "" + value, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
