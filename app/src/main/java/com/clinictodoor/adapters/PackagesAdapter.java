package com.clinictodoor.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.dialogs.AddToCartDialog;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.fragments.PackageDetailsFragment;
import com.clinictodoor.fragments.ServiceDetailsFragment;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.PackagesViewHolder> {

    Context ctx;
    List<PackagesDto> list;
    List<AddressDto> addresses_list;

    public PackagesAdapter(Context ctx, List<PackagesDto> list, List<AddressDto> addresses_list) {
        this.ctx = ctx;
        this.list = list;
        this.addresses_list = addresses_list;
    }

    @Override
    public PackagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_packages = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_service_item, parent, false);
        return new PackagesViewHolder(view_packages);
    }

    @Override
    public void onBindViewHolder(PackagesViewHolder holder, int position) {

        holder.tv_testName.setText(list.get(position).getTitle());
        holder.tv_amount.setText(ctx.getResources().getString(R.string.Price) + " " + list.get(position).getPrice());
        holder.tv_description.setText(Html.fromHtml(list.get(position).getDescription()));
        if (list.get(position).getIcon() != null) {
            holder.img_service.setImageResource(list.get(position).getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PackagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_testName, tv_amount, tv_description, tv_viewMore, tv_addToCart;
        ImageView img_service;

        public PackagesViewHolder(View itemView) {
            super(itemView);

            tv_testName = itemView.findViewById(R.id.tv_testName);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_viewMore = itemView.findViewById(R.id.tv_viewMore);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_addToCart = itemView.findViewById(R.id.tv_addToCart);
            img_service = itemView.findViewById(R.id.img_service);


            tv_testName.setTypeface(Utils.mTypeface(ctx, 4));
            tv_amount.setTypeface(Utils.mTypeface(ctx, 4));
            tv_viewMore.setTypeface(Utils.mTypeface(ctx, 4));
            tv_addToCart.setTypeface(Utils.mTypeface(ctx, 4));
            tv_description.setTypeface(Utils.mTypeface(ctx, 3));

            tv_addToCart.setOnClickListener(this);
            tv_viewMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_addToCart:
                    CartDto dto = new CartDto();
                    dto.setService_id(list.get(getPosition()).getService_id());
                    dto.setService_name(list.get(getPosition()).getTitle());
                    dto.setTest_id(list.get(getPosition()).getId());
                    dto.setPackage_type("null");
                    dto.setService_image(list.get(getPosition()).getPrice());
                    new AddToCartDialog(ctx, dto, addresses_list).show();
                    break;
                case R.id.tv_viewMore:
                    Fragment fragment = PackageDetailsFragment.newInstance("");
                    Bundle args = new Bundle();
                    args.putString("package_id", list.get(getPosition()).getId());
                    args.putInt("icon", list.get(getPosition()).getIcon());
                    fragment.setArguments(args);
                    FragmentTransaction fm = ((HomeActivity) ctx).getSupportFragmentManager().beginTransaction();
                    fm.addToBackStack("1").commit();
                    fm.replace(R.id.content_frame, fragment, null);
                    break;
            }
        }
    }
}
