package com.clinictodoor.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateFamilyMemberActivity;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.utils.Utils;

import java.util.List;

/**
 * Created by lenovo on 6/10/2018.
 */

public class FamilyMembersAdapter extends RecyclerView.Adapter<FamilyMembersAdapter.MyViewHolder> {

    Context ctx;
    List<FamilyMembersDto> list;

    public FamilyMembersAdapter(Context ctx, List<FamilyMembersDto> list) {
        this.ctx = ctx;
        this.list = list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.family_member_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_relationship.setText(list.get(position).getRelationship());
        holder.tv_name.setText(list.get(position).getSalutation() + " " + list.get(position).getFirst_name() + " " + list.get(position).getLast_name());
        holder.tv_mobile.setText("Mobile : " + list.get(position).getMobile());
        holder.tv_gender.setText(list.get(position).getGender());
//        holder.tv_address.setText("Address :\n " + list.get(position).getDoor_no() + "," + list.get(position).getStreet() + "," + list.get(position).getLandmark() + ",\n" + list.get(position).getCity() + "," + list.get(position).getState() + "," + list.get(position).getCountry() + "-" + list.get(position).getPin_code());
        if (list.get(position).getDob() != null) {
            String[] dob = list.get(position).getDob().split("-");
            String age = Utils.getAge(Integer.parseInt(dob[0]), Integer.parseInt(dob[1]), Integer.parseInt(dob[2]));
            holder.tv_dob.setText(age);
            if (Integer.parseInt(age) < 13) {
                if (list.get(position).getGender().equals("Male")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_below12male);
                } else if (list.get(position).getGender().equals("Female")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_below12female);
                }
            } else if (Integer.parseInt(age) < 13 && Integer.parseInt(age) < 25) {
                if (list.get(position).getGender().equals("Male")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_btw13_25male);
                } else if (list.get(position).getGender().equals("Female")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_btw13_25female);
                }
            } else if (Integer.parseInt(age) < 25 && Integer.parseInt(age) < 55) {
                if (list.get(position).getGender().equals("Male")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_btw26_55male);
                } else if (list.get(position).getGender().equals("Female")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_btw26_55female);
                }
            } else if (Integer.parseInt(age) > 55) {
                if (list.get(position).getGender().equals("Male")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_above55male);
                } else if (list.get(position).getGender().equals("Female")) {
                    holder.img_relation.setImageResource(R.mipmap.ic_above55female);
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_relationship, tv_name, tv_mobile, tv_dob, tv_gender, tv_address/*, tv_editMember, tv_deleteMember*/;
        ImageView img_editMember, img_relation;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_relationship = itemView.findViewById(R.id.tv_relationship);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_mobile = itemView.findViewById(R.id.tv_mobile);
            tv_dob = itemView.findViewById(R.id.tv_dob);
            tv_gender = itemView.findViewById(R.id.tv_gender);
            tv_address = itemView.findViewById(R.id.tv_address);
            img_editMember = itemView.findViewById(R.id.img_editMember);
            img_relation = itemView.findViewById(R.id.img_relation);

            tv_relationship.setTypeface(Utils.mTypeface(ctx, 4));
            tv_name.setTypeface(Utils.mTypeface(ctx, 3));
            tv_mobile.setTypeface(Utils.mTypeface(ctx, 3));
            tv_dob.setTypeface(Utils.mTypeface(ctx, 3));
            tv_gender.setTypeface(Utils.mTypeface(ctx, 3));
            tv_address.setTypeface(Utils.mTypeface(ctx, 3));


            img_editMember.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_editMember:
                    Intent in_edit = new Intent(ctx, CreateFamilyMemberActivity.class);
                    in_edit.putExtra("family_members", list.get(getPosition()));
                    ctx.startActivity(in_edit);
                    break;
            }
        }
    }
}
