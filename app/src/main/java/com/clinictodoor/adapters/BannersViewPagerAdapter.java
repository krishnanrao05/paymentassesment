package com.clinictodoor.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.clinictodoor.R;

import java.util.List;

/**
 * Created by lenovo on 5/31/2018.
 */

public class BannersViewPagerAdapter extends PagerAdapter {

    Context ctx;
    List<Integer> list_images;
    LayoutInflater inflater;

    public BannersViewPagerAdapter(Context ctx, List<Integer> list_images) {
        this.ctx = ctx;
        this.list_images = list_images;
        inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return list_images.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View view_layout = inflater.inflate(R.layout.banner_item, view, false);
        assert view_layout != null;
        ImageView img_banner = view_layout.findViewById(R.id.img_banner);
        img_banner.setImageResource(list_images.get(position));
        view.addView(view_layout, 0);
        return view_layout;
    }
}
