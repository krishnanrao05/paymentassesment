package com.clinictodoor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class AddressesSpinnerAdapter extends ArrayAdapter<String> {
    // CREATING THE CUSTOM SPINNER FOR THE TEXT CHANGES AND FOR THE TYPEFACE
    Context ctx;
    List<AddressDto> list;
//    Typeface tfr;

    public AddressesSpinnerAdapter(Context ctx, int textViewResourceId, List<AddressDto> list) {
        super(ctx, textViewResourceId);
        this.ctx = ctx;
        this.list = list;

//        tfr = Typeface.createFromAsset(ctx.getAssets(), "fonts/robotoregular.ttf");
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class ViewHolder {
        TextView tvsp;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.spinner_item, parent, false);
            holder = new ViewHolder();
            holder.tvsp = (TextView) vi.findViewById(R.id.tvsp);

            holder.tvsp.setTypeface(Utils.mTypeface(ctx, 4));
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        holder.tvsp.setText(list.get(position).getAddress_type());
        return vi;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.spinner_item, parent, false);
            holder = new ViewHolder();
            holder.tvsp = (TextView) vi.findViewById(R.id.tvsp);
            holder.tvsp.setTextColor(ctx.getResources().getColor(R.color.black_color));
            holder.tvsp.setTypeface(Utils.mTypeface(ctx, 4));
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        holder.tvsp.setText(list.get(position).getAddress_type());
        return vi;

    }
}
