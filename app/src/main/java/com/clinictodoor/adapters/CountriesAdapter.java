package com.clinictodoor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.dtos.CountriesDto;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountriesViewHolder> {

    Context ctx;
    List<CountriesDto> list_country;

    public CountriesAdapter(Context ctx, List<CountriesDto> list_country) {
        this.ctx = ctx;
        this.list_country = list_country;
    }

    @Override
    public CountriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_countries = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_item, parent, false);
        return new CountriesViewHolder(view_countries);
    }

    @Override
    public void onBindViewHolder(CountriesViewHolder holder, int position) {

        holder.tv_countryCode.setText(list_country.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return list_country.size();
    }

    public class CountriesViewHolder extends RecyclerView.ViewHolder {

        TextView tv_countryCode;

        public CountriesViewHolder(View itemView) {
            super(itemView);
            tv_countryCode = itemView.findViewById(R.id.tv_countryCode);

            tv_countryCode.setTypeface(Utils.mTypeface(ctx, 4));

        }
    }
}
