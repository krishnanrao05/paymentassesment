package com.clinictodoor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class TestsDialogAdapter extends RecyclerView.Adapter<TestsDialogAdapter.TestsViewHolder> {

    Context ctx;
    List<PackagesDto> list;

    public TestsDialogAdapter(Context ctx, List<PackagesDto> list) {
        this.ctx = ctx;
        this.list = list;
    }

    @Override
    public TestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_tests = LayoutInflater.from(parent.getContext()).inflate(R.layout.packages_item, parent, false);
        return new TestsViewHolder(view_tests);
    }

    @Override
    public void onBindViewHolder(TestsViewHolder holder, int position) {
        holder.tv_packageName.setText(list.get(position).getTitle());
        holder.tv_price.setText(ctx.getResources().getString(R.string.Price) + " " + list.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TestsViewHolder extends RecyclerView.ViewHolder {

        TextView tv_packageName, tv_price;

        public TestsViewHolder(View itemView) {
            super(itemView);
            tv_packageName = itemView.findViewById(R.id.tv_packageName);
            tv_price = itemView.findViewById(R.id.tv_price);

            tv_packageName.setTypeface(Utils.mTypeface(ctx, 4));
            tv_price.setTypeface(Utils.mTypeface(ctx, 4));
        }
    }
}
