package com.clinictodoor.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dialogs.AddToCartDialog;
import com.clinictodoor.dialogs.PackagesDialog;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.fragments.C2DFragment;
import com.clinictodoor.fragments.PackagesFragment;
import com.clinictodoor.fragments.ServiceDetailsFragment;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class BookServicesAdapter extends RecyclerView.Adapter<BookServicesAdapter.BookServicesViewHolder> implements ResponseListner {


    Context ctx;
    List<ServicesDto> list_services;
    ProfileDto profile_dto;
    List<AddressDto> list_addresses;

    public BookServicesAdapter(Context ctx, List<ServicesDto> list_services, ProfileDto profile_dto, List<AddressDto> list_addresses) {
        this.ctx = ctx;
        this.list_services = list_services;
        this.profile_dto = profile_dto;
        this.list_addresses = list_addresses;
    }

    @Override
    public BookServicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_bookServices = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_service_item, parent, false);
        return new BookServicesViewHolder(view_bookServices);
    }

    @Override
    public void onBindViewHolder(BookServicesViewHolder holder, int position) {
        holder.tv_testName.setText(list_services.get(position).getTitle());
        holder.tv_amount.setText(ctx.getResources().getString(R.string.Price) + " " + list_services.get(position).getPrice());
        holder.tv_description.setText(list_services.get(position).getDescription());
        holder.img_service.setImageResource(list_services.get(position).getIcon());

        if (list_services.get(position).getPrice().equals("0")) {
            holder.tv_amount.setVisibility(View.GONE);
        } else {
            holder.tv_amount.setVisibility(View.VISIBLE);
        }

//
//        if (list_services.get(position).getHas_multiple_tests().equals("1")) {
//            holder.tv_addToCart.setText("View Tests");
//        } else if (list_services.get(position).getHas_multiple_packages().equals("1")) {
//            holder.tv_addToCart.setText("View Packages");
//        } else {
//            holder.tv_addToCart.setText("Add To Cart");
//        }
    }

    @Override
    public int getItemCount() {
        return list_services.size();
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetPackagesUrl)) {
            List<PackagesDto> list_packages = jsonParser.mPackagesList(response);
            if (list_packages.size() != 0) {
                Fragment fragment = PackagesFragment.newInstance("");
                Bundle args = new Bundle();
                args.putSerializable("packages_list", (Serializable) list_packages);
                args.putSerializable("addresses_list", (Serializable) list_addresses);
                args.putString("hint", "packages");
                fragment.setArguments(args);
                FragmentTransaction fm = ((HomeActivity) ctx).getSupportFragmentManager().beginTransaction();
                fm.addToBackStack("1").commit();
                fm.replace(R.id.content_frame, fragment, null);
            }
        } else if (path.contains(Constants.GetTestsUrl)) {
            List<PackagesDto> list_packages = jsonParser.mTestsList(response);
            if (list_packages.size() != 0) {
                new PackagesDialog(ctx, list_packages, list_addresses).show();
            }
        }
    }

    public class BookServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_testName, tv_amount, tv_description, tv_viewMore, tv_addToCart;
        ImageView img_service;

        public BookServicesViewHolder(View itemView) {
            super(itemView);

            tv_testName = itemView.findViewById(R.id.tv_testName);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_viewMore = itemView.findViewById(R.id.tv_viewMore);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_addToCart = itemView.findViewById(R.id.tv_addToCart);
            img_service = itemView.findViewById(R.id.img_service);


            tv_testName.setTypeface(Utils.mTypeface(ctx, 4));
            tv_amount.setTypeface(Utils.mTypeface(ctx, 4));
            tv_viewMore.setTypeface(Utils.mTypeface(ctx, 4));
            tv_addToCart.setTypeface(Utils.mTypeface(ctx, 4));
            tv_description.setTypeface(Utils.mTypeface(ctx, 3));

            tv_addToCart.setOnClickListener(this);
            tv_viewMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_addToCart:
                    if (list_addresses.size() == 0) {
                        Utils.customToast(ctx, "Please add at least one address for Adding Service to Cart.");
                    } else if (profile_dto.getId() == null) {
                        Utils.customToast(ctx, "Please create your profile for Adding Service to Cart.");
                    } else if (list_addresses != null && list_addresses.size() == 0) {
                        Utils.customToast(ctx, "Please add at least one address for Adding Item to Cart.");
                    } else if (list_services.get(getPosition()).getHas_fixed_price().equals("0")) {
                        if (list_services.get(getPosition()).getHas_multiple_packages().equals("1")) {
                            try {
                                AsynTask as_packages = new AsynTask(ctx, new URL(Constants.BaseUrl + "services/" + list_services.get(getPosition()).getId() + "/" + Constants.GetPackagesUrl), BookServicesAdapter.this, false);
                                as_packages.execute("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (list_services.get(getPosition()).getHas_multiple_tests().equals("1")) {
                            try {
                                AsynTask as_packages = new AsynTask(ctx, new URL(Constants.BaseUrl + "services/" + list_services.get(getPosition()).getId() + "/" + Constants.GetTestsUrl), BookServicesAdapter.this, false);
                                as_packages.execute("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        CartDto dto = new CartDto();
                        dto.setService_id(list_services.get(getPosition()).getId());
                        dto.setService_name(list_services.get(getPosition()).getTitle());
                        dto.setPackage_type("null");
                        dto.setTest_id("null");
                        dto.setService_image(list_services.get(getPosition()).getPrice());
                        new AddToCartDialog(ctx, dto, list_addresses).show();
                    }
                    break;
                case R.id.tv_viewMore:
                    Fragment fragment = ServiceDetailsFragment.newInstance("");
                    Bundle args = new Bundle();
                    args.putString("service_id", list_services.get(getPosition()).getId());
                    args.putInt("icon", list_services.get(getPosition()).getIcon());
                    fragment.setArguments(args);
                    FragmentTransaction fm = ((HomeActivity) ctx).getSupportFragmentManager().beginTransaction();
                    fm.addToBackStack("1").commit();
                    fm.replace(R.id.content_frame, fragment, null);
                    break;
            }
        }
    }
}
