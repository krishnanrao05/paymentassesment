package com.clinictodoor.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.fragments.ServiceDetailsFragment;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class NavigationServicesAdapter extends RecyclerView.Adapter<NavigationServicesAdapter.NavigationServicesViewHolder> {

    Context ctx;
    List<ServicesDto> list_services;
    DrawerLayout drawer_layout;

    public NavigationServicesAdapter(Context ctx, List<ServicesDto> list_services, DrawerLayout drawer_layout) {
        this.ctx = ctx;
        this.list_services = list_services;
        this.drawer_layout = drawer_layout;
    }

    @Override
    public NavigationServicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view_navigation = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_services_item, parent, false);
        return new NavigationServicesViewHolder(view_navigation);
    }

    @Override
    public void onBindViewHolder(NavigationServicesViewHolder holder, int position) {
        holder.img_serviceIcon.setImageResource(list_services.get(position).getIcon());
        holder.tv_serviceName.setText(list_services.get(position).getTitle());

        holder.tv_serviceName.setTypeface(Utils.mTypeface(ctx, 3));
    }

    @Override
    public int getItemCount() {
        return list_services.size();
    }

    public class NavigationServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView img_serviceIcon;
        TextView tv_serviceName;

        public NavigationServicesViewHolder(View itemView) {
            super(itemView);
            img_serviceIcon = itemView.findViewById(R.id.img_serviceIcon);
            tv_serviceName = itemView.findViewById(R.id.tv_serviceName);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            drawer_layout.closeDrawer(Gravity.START);
            Fragment fragment = ServiceDetailsFragment.newInstance("");
            Bundle args = new Bundle();
            args.putString("service_id", list_services.get(getPosition()).getId());
            args.putInt("icon", list_services.get(getPosition()).getIcon());
            fragment.setArguments(args);
            FragmentTransaction fm = ((HomeActivity) ctx).getSupportFragmentManager().beginTransaction();
            fm.addToBackStack("1").commit();
            fm.replace(R.id.content_frame, fragment, null);
        }
    }
}
