package com.clinictodoor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.utils.Utils;

/**
 * Created by lenovo on 6/1/2018.
 */

public class MyAppointmentsAdapter extends RecyclerView.Adapter<MyAppointmentsAdapter.MyViewHolder> {

    Context ctx;

    public MyAppointmentsAdapter(Context ctx) {
        this.ctx = ctx;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_appointments_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        holder.tv_bookedServiceName.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_date.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_time.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_bookFor.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_name.setTypeface(Utils.mTypeface(ctx, 3));
        holder.address.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_address.setTypeface(Utils.mTypeface(ctx, 3));
        holder.tv_view.setTypeface(Utils.mTypeface(ctx, 3));
        holder.tv_rescheadule.setTypeface(Utils.mTypeface(ctx, 3));
        holder.tv_cancel.setTypeface(Utils.mTypeface(ctx, 3));

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_bookedServiceName, tv_date, tv_time, tv_bookFor, tv_name, address, tv_address, tv_view, tv_rescheadule, tv_cancel;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_bookedServiceName = itemView.findViewById(R.id.tv_bookedServiceName);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_bookFor = itemView.findViewById(R.id.tv_bookFor);
            tv_name = itemView.findViewById(R.id.tv_name);
            address = itemView.findViewById(R.id.address);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_view = itemView.findViewById(R.id.tv_view);
            tv_rescheadule = itemView.findViewById(R.id.tv_rescheadule);
            tv_cancel = itemView.findViewById(R.id.tv_cancel);

        }
    }
}
