package com.clinictodoor.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.fragments.C2DFragment;
import com.clinictodoor.fragments.ServiceDetailsFragment;
import com.clinictodoor.utils.Utils;

import java.util.List;

/**
 * Created by lenovo on 6/10/2018.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.MyViewHolder> {

    Context ctx;
    List<ServicesDto> list_services;

    public ServicesAdapter(Context ctx, List<ServicesDto> list_services) {
        this.ctx = ctx;
        this.list_services = list_services;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.img_icon.setImageResource(list_services.get(position).getIcon());
        holder.tv_title.setText(list_services.get(position).getTitle());

        if (list_services.get(position).getHas_fixed_price().equals("1")) {
            holder.tv_price.setText(ctx.getResources().getString(R.string.Price) + " " + list_services.get(position).getPrice());
            holder.img_cart.setVisibility(View.VISIBLE);
            holder.tv_price.setVisibility(View.VISIBLE);
            holder.tv_Support.setVisibility(View.GONE);
        } else {
            holder.img_cart.setVisibility(View.GONE);
            holder.tv_price.setVisibility(View.GONE);
            holder.tv_Support.setVisibility(View.VISIBLE);
            holder.tv_Support.setText("More Details");
        }

        holder.tv_title.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_price.setTypeface(Utils.mTypeface(ctx, 4));
        holder.tv_Support.setTypeface(Utils.mTypeface(ctx, 4));
    }

    @Override
    public int getItemCount() {
        return list_services.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_icon, img_cart;
        TextView tv_title, tv_Support, tv_price;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_icon = itemView.findViewById(R.id.img_icon);
            img_cart = itemView.findViewById(R.id.img_cart);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_Support = itemView.findViewById(R.id.tv_Support);
            tv_price = itemView.findViewById(R.id.tv_price);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Fragment fragment = ServiceDetailsFragment.newInstance("");
            Bundle args = new Bundle();
            args.putString("service_id", list_services.get(getPosition()).getId());
            args.putInt("icon", list_services.get(getPosition()).getIcon());
            fragment.setArguments(args);
            FragmentTransaction fm = ((HomeActivity) ctx).getSupportFragmentManager().beginTransaction();
            fm.addToBackStack("1").commit();
            fm.replace(R.id.content_frame, fragment, null);
        }
    }
}
