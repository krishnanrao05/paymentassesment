package com.clinictodoor.activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;
import com.clinictodoor.adapters.NavigationServicesAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.database.DBHelper;
import com.clinictodoor.dialogs.CitiesDialog;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.fragments.AboutFragment;
import com.clinictodoor.fragments.BookServiceFragment;
import com.clinictodoor.fragments.C2DFragment;
import com.clinictodoor.fragments.FamilyMembersFragment;
import com.clinictodoor.fragments.MyAddressesFragment;
import com.clinictodoor.fragments.MyAppointmentsFragment;
import com.clinictodoor.fragments.MyReportsFragment;
import com.clinictodoor.fragments.ProfileFragment;
import com.clinictodoor.fragments.UpdateProfileFragment;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.RoundedCornersTransform;
import com.clinictodoor.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, ResponseListner, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Fragment fragment = null;
    LinearLayout ll_profile, ll_genderAge, ll_location;
    TextView tv_location;
    Context ctx;
    ImageView img_menu, img_back, img_edit, img_profilePic, img_logo, img_cart;
    ProfileDto profile_dto;
    TextView tv_cartCount;

    String mobile;
    DrawerLayout drawer_layout;

    TextView tv_name, tv_gender, tv_age, tv_mobile, tv_emrNo;
    RelativeLayout rel_profile;
    LinearLayout ll_home, ll_myProfile, ll_myFamily, ll_myAppointments, ll_myAddresses, ll_ourServices, ll_about, ll_rateUs, ll_signOut, ll_login, ll_signUp;
    TextView tv_home, tv_myProfile, tv_myFamily, tv_myAppointments, tv_myAddresses, tv_ourServices, tv_about, tv_rateUs, tv_signOut, tv_login, tv_number, tv_signUp;
    RecyclerView rec_services;
    ImageView img_arrow;
    NavigationServicesAdapter navigationServicesAdapter;
    boolean isHideServicesView = false;

    //    FooterMenu Items
    LinearLayout ll_myReports, ll_bookAService, ll_myApnts;
    ImageView img_reports, img_bookServices, img_appointments;
    TextView tv_myReports, tv_bookService, tv_myApnts;

    BroadcastReceiver br;
    AlarmManager am;
    PendingIntent pi;


    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    LocationManager manager;

    Geocoder geocoder;
    List<Address> addresses;
    String city = "", state;

    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);
        ctx = this;

        db = new DBHelper(ctx);

        setup();
        drawer_layout = findViewById(R.id.drawer_layout);
        NavigationView nav_menu = findViewById(R.id.nav_menu);
        View headerLayout = nav_menu.getHeaderView(0);

        img_logo = headerLayout.findViewById(R.id.img_logo);
        ll_login = headerLayout.findViewById(R.id.ll_login);
        ll_signUp = headerLayout.findViewById(R.id.ll_signUp);
        tv_signUp = headerLayout.findViewById(R.id.tv_signUp);
        img_back = headerLayout.findViewById(R.id.img_back);
        img_edit = headerLayout.findViewById(R.id.img_edit);
        img_profilePic = headerLayout.findViewById(R.id.img_profilePic);
        tv_name = headerLayout.findViewById(R.id.tv_name);
        tv_gender = headerLayout.findViewById(R.id.tv_gender);
        tv_age = headerLayout.findViewById(R.id.tv_age);
        tv_mobile = headerLayout.findViewById(R.id.tv_mobile);
        tv_emrNo = headerLayout.findViewById(R.id.tv_emrNo);
        ll_profile = headerLayout.findViewById(R.id.ll_profile);
        ll_signOut = headerLayout.findViewById(R.id.ll_signOut);
        rel_profile = headerLayout.findViewById(R.id.rel_profile);
        ll_home = headerLayout.findViewById(R.id.ll_home);
        ll_myProfile = headerLayout.findViewById(R.id.ll_myProfile);
        ll_myFamily = headerLayout.findViewById(R.id.ll_myFamily);
        ll_myAppointments = headerLayout.findViewById(R.id.ll_myAppointments);
        ll_myAddresses = headerLayout.findViewById(R.id.ll_myAddresses);
        ll_ourServices = headerLayout.findViewById(R.id.ll_ourServices);
        img_arrow = headerLayout.findViewById(R.id.img_arrow);
        ll_about = headerLayout.findViewById(R.id.ll_about);
        ll_rateUs = headerLayout.findViewById(R.id.ll_rateUs);
        tv_home = headerLayout.findViewById(R.id.tv_home);
        tv_myProfile = headerLayout.findViewById(R.id.tv_myProfile);
        tv_myFamily = headerLayout.findViewById(R.id.tv_myFamily);
        tv_myAppointments = headerLayout.findViewById(R.id.tv_myAppointments);
        tv_myAddresses = headerLayout.findViewById(R.id.tv_myAddresses);
        tv_ourServices = headerLayout.findViewById(R.id.tv_ourServices);
        tv_about = headerLayout.findViewById(R.id.tv_about);
        tv_rateUs = headerLayout.findViewById(R.id.tv_rateUs);
        tv_signOut = headerLayout.findViewById(R.id.tv_signOut);
        tv_login = headerLayout.findViewById(R.id.tv_login);
        ll_genderAge = headerLayout.findViewById(R.id.ll_genderAge);
        rec_services = headerLayout.findViewById(R.id.rec_services);
        rec_services.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        tv_number = findViewById(R.id.tv_number);
        ll_myReports = findViewById(R.id.ll_myReports);
        ll_bookAService = findViewById(R.id.ll_bookAService);
        ll_myApnts = findViewById(R.id.ll_myApnts);
        img_menu = findViewById(R.id.img_menu);
        img_cart = findViewById(R.id.img_cart);
        tv_location = findViewById(R.id.tv_location);
        ll_location = findViewById(R.id.ll_location);
        img_reports = findViewById(R.id.img_reports);
        img_bookServices = findViewById(R.id.img_bookServices);
        img_appointments = findViewById(R.id.img_appointments);
        tv_cartCount = findViewById(R.id.tv_cartCount);

        tv_myReports = findViewById(R.id.tv_myReports);
        tv_bookService = findViewById(R.id.tv_bookService);
        tv_myApnts = findViewById(R.id.tv_myApnts);


        fragment = C2DFragment.newInstance("");
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        fm.addToBackStack("1").commit();
        fm.replace(R.id.content_frame, fragment, null);


        ll_myReports.setOnClickListener(this);
        ll_bookAService.setOnClickListener(this);
        ll_myApnts.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        ll_login.setOnClickListener(this);
        ll_signUp.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_edit.setOnClickListener(this);
        ll_signOut.setOnClickListener(this);
        rel_profile.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        ll_ourServices.setOnClickListener(this);
        ll_about.setOnClickListener(this);
        ll_rateUs.setOnClickListener(this);
        ll_myProfile.setOnClickListener(this);
        ll_myFamily.setOnClickListener(this);
        ll_myAppointments.setOnClickListener(this);
        ll_myAddresses.setOnClickListener(this);
        img_profilePic.setOnClickListener(this);
        ll_location.setOnClickListener(this);
        img_cart.setOnClickListener(this);

        tv_name.setTypeface(Utils.mTypeface(ctx, 4));
        tv_gender.setTypeface(Utils.mTypeface(ctx, 4));
        tv_age.setTypeface(Utils.mTypeface(ctx, 4));
        tv_mobile.setTypeface(Utils.mTypeface(ctx, 4));
        tv_emrNo.setTypeface(Utils.mTypeface(ctx, 4));
        tv_home.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myProfile.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myFamily.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myAppointments.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myAddresses.setTypeface(Utils.mTypeface(ctx, 4));
        tv_ourServices.setTypeface(Utils.mTypeface(ctx, 4));
        tv_about.setTypeface(Utils.mTypeface(ctx, 4));
        tv_rateUs.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myReports.setTypeface(Utils.mTypeface(ctx, 3));
        tv_bookService.setTypeface(Utils.mTypeface(ctx, 3));
        tv_myApnts.setTypeface(Utils.mTypeface(ctx, 3));
        tv_signOut.setTypeface(Utils.mTypeface(ctx, 4));
        tv_login.setTypeface(Utils.mTypeface(ctx, 4));
        tv_number.setTypeface(Utils.mTypeface(ctx, 4));
        tv_location.setTypeface(Utils.mTypeface(ctx, 3));
        tv_signUp.setTypeface(Utils.mTypeface(ctx, 4));


        geocoder = new Geocoder(ctx, Locale.getDefault());

        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        manager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);


        if (Utils.isGPSEnabled(ctx)) {
            ll_location.setVisibility(View.VISIBLE);
        } else {
            ll_location.setVisibility(View.GONE);
            SharedPreferences sharedpreferences = getSharedPreferences("location", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("city", "");
            editor.commit();

            new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    Intent in_noGps = new Intent(HomeActivity.this, NoGpsActivity.class);
                    startActivity(in_noGps);
                }

            }.start();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_menu:
                drawer_layout.openDrawer(Gravity.START);
                break;
            case R.id.img_back:
                drawer_layout.closeDrawer(Gravity.START);
                break;
            case R.id.img_edit:
                drawer_layout.closeDrawer(Gravity.START);
                Intent in_profile = new Intent(HomeActivity.this, CreateProfileActivity.class);
                in_profile.putExtra("profile", profile_dto);
                startActivity(in_profile);
                break;
            case R.id.img_cart:
                Intent in_cart = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(in_cart);
                break;
            case R.id.ll_signUp:
                drawer_layout.closeDrawer(Gravity.START);
                if (Utils.isGPSEnabled(ctx)) {
                    Intent in_signUp = new Intent(HomeActivity.this, SignUpActivity.class);
                    startActivity(in_signUp);
                } else {
                    Utils.customToast(ctx, "Please enable GPS...");
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
//                    Toast.makeText(ctx, "Please enable GPS...", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_login:
                drawer_layout.closeDrawer(Gravity.START);
                Intent in_login = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(in_login);
                break;
            case R.id.ll_signOut:
                SharedPreferences sharedpreferences = getSharedPreferences("login_access", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("access_token", "");
                editor.putString("refresh_token", "");
                editor.putString("profile_date", "");
                editor.putString("mobile", "");
                editor.commit();
                drawer_layout.closeDrawer(Gravity.START);
                Intent in_signOut = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(in_signOut);
                finish();
                break;
            case R.id.img_profilePic:
                drawer_layout.closeDrawer(Gravity.START);
                if (!Utils.checkPermission(ctx)) {
                    Utils.requestPermission(HomeActivity.this);
                } else if (profile_dto == null) {
                    Utils.customToast(ctx, "Please create your profile first.");
                } else {
                    fragment = UpdateProfileFragment.newInstance("");
                    FragmentTransaction fm_update = getSupportFragmentManager().beginTransaction();
                    fm_update.addToBackStack("1").commit();
                    fm_update.replace(R.id.content_frame, fragment, null);
                }
                break;
            case R.id.ll_myProfile:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = ProfileFragment.newInstance("");
                FragmentTransaction fm_profile = getSupportFragmentManager().beginTransaction();
                fm_profile.addToBackStack("1").commit();
                fm_profile.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_myFamily:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = FamilyMembersFragment.newInstance("");
                FragmentTransaction fm_family = getSupportFragmentManager().beginTransaction();
                fm_family.addToBackStack("1").commit();
                fm_family.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_myAppointments:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = MyAppointmentsFragment.newInstance("");
                FragmentTransaction fm_myApts = getSupportFragmentManager().beginTransaction();
                fm_myApts.addToBackStack("1").commit();
                fm_myApts.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_myAddresses:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = MyAddressesFragment.newInstance("");
                FragmentTransaction fm_myAddress = getSupportFragmentManager().beginTransaction();
                fm_myAddress.addToBackStack("1").commit();
                fm_myAddress.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_myApnts:
                footerBtnClick();
                fragment = MyAppointmentsFragment.newInstance("");
                FragmentTransaction fm_myApts2 = getSupportFragmentManager().beginTransaction();
                fm_myApts2.addToBackStack("1").commit();
                fm_myApts2.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_myReports:
                footerBtnClick();
                fragment = C2DFragment.newInstance("");
                FragmentTransaction fm_reports = getSupportFragmentManager().beginTransaction();
                fm_reports.addToBackStack("1").commit();
                fm_reports.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_bookAService:
                footerBtnClick();
                fragment = BookServiceFragment.newInstance("");
                FragmentTransaction fm_bookService = getSupportFragmentManager().beginTransaction();
                fm_bookService.addToBackStack("1").commit();
                fm_bookService.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_home:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = C2DFragment.newInstance("");
                FragmentTransaction fm_home = getSupportFragmentManager().beginTransaction();
                fm_home.addToBackStack("1").commit();
                fm_home.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_ourServices:
                ourServicesBtnClick();
                if (isHideServicesView == false) {
                    img_arrow.setImageResource(R.mipmap.ic_up_arrow);
                    rec_services.setVisibility(View.VISIBLE);
                    isHideServicesView = true;
                } else {
                    img_arrow.setImageResource(R.mipmap.ic_down_arrow);
                    ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
                    rec_services.setVisibility(View.GONE);
                    isHideServicesView = false;
                }
                break;
            case R.id.ll_about:
                drawer_layout.closeDrawer(Gravity.START);
                fragment = AboutFragment.newInstance("");
                FragmentTransaction fm_about = getSupportFragmentManager().beginTransaction();
                fm_about.addToBackStack("1").commit();
                fm_about.replace(R.id.content_frame, fragment, null);
                break;
            case R.id.ll_rateUs:

                break;
            case R.id.ll_location:
                new CitiesDialog(ctx, "home").show();
                break;
        }
    }

    public void homeBtnClick() {
//        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
//        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
//        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
//        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void myProfileBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void myFamilyBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void aboutBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void LocationName(String city_name) {
        city = city_name;
        if (city.equals("")) {
            ll_location.setVisibility(View.GONE);
        } else {
            ll_location.setVisibility(View.VISIBLE);
            tv_location.setText(city_name);
        }
    }

    public void ourServicesBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    private void footerBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void myAppointmentsBtnClick() {
        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#00000000"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void myAddressesBtnClick() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));

        ll_home.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myProfile.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myFamily.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAppointments.setBackgroundColor(Color.parseColor("#00000000"));
        ll_myAddresses.setBackgroundColor(Color.parseColor("#22FFFFFF"));
        ll_ourServices.setBackgroundColor(Color.parseColor("#00000000"));
        ll_about.setBackgroundColor(Color.parseColor("#00000000"));
        ll_rateUs.setBackgroundColor(Color.parseColor("#00000000"));
    }

    public void myAppointmentsItems() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.white_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.white_color));
    }

    public void bookAServiceItems() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.white_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.white_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
    }

    public void reportsItems() {
        img_reports.setColorFilter(ContextCompat.getColor(ctx, R.color.white_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_bookServices.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        img_appointments.setColorFilter(ContextCompat.getColor(ctx, R.color.sideMenuText_color), android.graphics.PorterDuff.Mode.SRC_IN);
        tv_myReports.setTextColor(ctx.getResources().getColor(R.color.white_color));
        tv_bookService.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
        tv_myApnts.setTextColor(ctx.getResources().getColor(R.color.sideMenuText_color));
    }

    @Override
    public void onBackPressed() {
        int cnt = getSupportFragmentManager().getBackStackEntryCount();
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            if (cnt <= 1) {
                getSupportFragmentManager().popBackStack();
                finish();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        List<CartDto> list_cart = db.getCartList();

        if (list_cart.size() != 0) {
            cartCount("" + list_cart.size());
        } else {
            cartCount("");
        }

        if (city.equals("")) {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl), HomeActivity.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl + "/" + city), HomeActivity.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("login_access", Context.MODE_PRIVATE);
        mobile = sharedPrefgcm.getString("mobile", "");

        if (mobile.equals("")) {
            img_logo.setVisibility(View.VISIBLE);
            ll_login.setVisibility(View.VISIBLE);
            ll_signUp.setVisibility(View.VISIBLE);
            ll_profile.setVisibility(View.GONE);
            img_edit.setVisibility(View.GONE);
            tv_emrNo.setVisibility(View.GONE);
            ll_signOut.setVisibility(View.GONE);
            ll_myProfile.setVisibility(View.GONE);
            ll_myFamily.setVisibility(View.GONE);
            ll_myAddresses.setVisibility(View.GONE);
            ll_myAppointments.setVisibility(View.GONE);
        } else {
            try {
                AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), HomeActivity.this, false);
                as_profile.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (Utils.isGPSEnabled(ctx)) {
            mGoogleApiClient.connect();
        }
    }


    public void hideTransparentBar(int value) {
        if (value == 1) {
            Intent in_noGps = new Intent(HomeActivity.this, NoGpsActivity.class);
            startActivity(in_noGps);
        }
    }

    public void cartCount(String count) {
        if (count.equals("")) {
            tv_cartCount.setVisibility(View.GONE);
        } else {
            tv_cartCount.setVisibility(View.VISIBLE);
            tv_cartCount.setText(count);
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.SaveProfileUrl)) {
            JsonParser jsonParser = new JsonParser();
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    if (jobj.has("user_profile")) {
                        JSONObject jobj_new = jobj.getJSONObject("user_profile");
                        if (jobj_new.has("emr_no")) {
                            tv_emrNo.setText("EMR NO. " + jobj_new.getString("emr_no"));
                        }
                        if (!jobj_new.getString("profile").equals("null")) {
                            profile_dto = jsonParser.mUserProfile(response);
                            img_logo.setVisibility(View.GONE);
                            ll_login.setVisibility(View.GONE);
                            ll_signUp.setVisibility(View.GONE);
                            ll_profile.setVisibility(View.VISIBLE);
                            img_edit.setVisibility(View.VISIBLE);
                            tv_emrNo.setVisibility(View.VISIBLE);
                            ll_signOut.setVisibility(View.VISIBLE);
                            ll_myAddresses.setVisibility(View.VISIBLE);
                            ll_myAppointments.setVisibility(View.VISIBLE);
                            String[] dob = profile_dto.getDob().split("-");
                            String age = Utils.getAge(Integer.parseInt(dob[0]), Integer.parseInt(dob[1]), Integer.parseInt(dob[2]));
                            tv_age.setText(age);

                            tv_name.setText(profile_dto.getFirst_name() + " " + profile_dto.getLast_name());
                            tv_gender.setText(profile_dto.getGender());
                            tv_mobile.setText(mobile);
                            Picasso.with(ctx).load(Constants.ImageBaseUrl + profile_dto.getPic())
                                    .placeholder(R.drawable.profile_pic_bg)
                                    .transform(new RoundedCornersTransform())
                                    .error(R.drawable.profile_pic_bg).into(img_profilePic);
                        } else {
                            img_logo.setVisibility(View.GONE);
                            ll_login.setVisibility(View.GONE);
                            ll_signUp.setVisibility(View.GONE);
                            ll_profile.setVisibility(View.VISIBLE);
                            img_edit.setVisibility(View.VISIBLE);
                            tv_emrNo.setVisibility(View.VISIBLE);
                            ll_myProfile.setVisibility(View.VISIBLE);
                            ll_myFamily.setVisibility(View.VISIBLE);
                            tv_name.setText("Create Profile");
                            tv_mobile.setText(mobile);
                            tv_age.setText("--");
                            tv_gender.setText("----");
                            Picasso.with(ctx).load(Constants.ImageBaseUrl + Constants.DefaultImageUrl)
                                    .placeholder(R.drawable.profile_pic_bg)
                                    .transform(new RoundedCornersTransform())
                                    .error(R.drawable.profile_pic_bg).into(img_profilePic);
                        }
                    }
                }
            }
        } else if (path.contains(Constants.GetServicesUrl)) {
            JsonParser jsonParser = new JsonParser();
            List<ServicesDto> list_services = jsonParser.mServicesList(response);

            if (list_services.size() != 0) {
                ((HomeActivity) ctx).hideTransparentBar(2);
                for (int i = 0; i < list_services.size(); i++) {
                    if (i == 0) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac);
                    } else if (i == 1) {
                        list_services.get(i).setIcon(R.drawable.ic_illlness_prevention);
                    } else if (i == 2) {
                        list_services.get(i).setIcon(R.drawable.ic_workplace_wellness);
                    } else if (i == 3) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac_screening_live);
                    } else if (i == 4) {
                        list_services.get(i).setIcon(R.drawable.ic_breast_cancer);
                    } else if (i == 5) {
                        list_services.get(i).setIcon(R.drawable.ic_lab_tests);
                    }
                }
                navigationServicesAdapter = new NavigationServicesAdapter(ctx, list_services, drawer_layout);
                rec_services.setAdapter(navigationServicesAdapter);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults.length > 0) {
                    boolean ext_storage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean write_storage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (ext_storage && write_storage) {
                        fragment = UpdateProfileFragment.newInstance("");
                        FragmentTransaction fm_update = getSupportFragmentManager().beginTransaction();
                        fm_update.addToBackStack("1").commit();
                        fm_update.replace(R.id.content_frame, fragment, null);
                    } else {
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null)));
                    }
                }
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            LatLng latitudeLong = new LatLng(currentLatitude, currentLongitude);
            AddressLine(latitudeLong);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public void AddressLine(LatLng latLng) {

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        String addressline1 = addresses.get(0).getAddressLine(0);
        String[] add1 = addressline1.split(",");

//        area = addresses.get(0).getSubLocality();
        city = addresses.get(0).getLocality();
        state = addresses.get(0).getAdminArea();
        String area = addresses.get(0).getSubAdminArea();
//        country = addresses.get(0).getCountryName();
//        pinCode = addresses.get(0).getPostalCode();

        tv_location.setText(city);

        SharedPreferences sharedpreferences = getSharedPreferences("location", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("city", city);
        editor.commit();


    }

    private void setup() {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent i) {
                String city_name = i.getStringExtra("city");
                LocationName(city_name);

                SharedPreferences sharedpreferences = getSharedPreferences("location", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("city", city_name);
                editor.commit();

                city = city_name;

                fragment = C2DFragment.newInstance("");
                FragmentTransaction fm_home = getSupportFragmentManager().beginTransaction();
                fm_home.addToBackStack("1").commit();
                fm_home.replace(R.id.content_frame, fragment, null);

            }
        };
        ctx.registerReceiver(br, new IntentFilter("sendBroadCast"));
        pi = PendingIntent.getBroadcast(ctx, 0, new Intent("sendBroadCast"), 0);
        am = (AlarmManager) (ctx.getSystemService(Context.ALARM_SERVICE));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ctx.unregisterReceiver(br);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}