package com.clinictodoor.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dialogs.CountriesDialog;
import com.clinictodoor.dtos.CountriesDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, ResponseListner, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    EditText edt_mobile, edt_pwd, edt_confirmPwd, edt_otp;
    LinearLayout ll_otp;
    TextView tv_signup, tv_login, tv_country, tv_signUpTitle;
    Context ctx;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    LocationManager manager;

    Geocoder geocoder;
    List<Address> addresses;
    String city, state, country_code = "", calling_code = "";
    LinearLayout ll_countryCode;


    public void gettingCodes(String countryCode, String callingCode) {
        country_code = countryCode;
        calling_code = callingCode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ctx = this;

        geocoder = new Geocoder(this, Locale.getDefault());

        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        manager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        tv_country = findViewById(R.id.tv_country);
        edt_mobile = findViewById(R.id.edt_mobile);
        edt_pwd = findViewById(R.id.edt_pwd);
        edt_confirmPwd = findViewById(R.id.edt_confirmPwd);
        edt_otp = findViewById(R.id.edt_otp);
        ll_otp = findViewById(R.id.ll_otp);
        tv_login = findViewById(R.id.tv_login);
        tv_signup = findViewById(R.id.tv_signup);
        ll_countryCode = findViewById(R.id.ll_countryCode);
        tv_signUpTitle = findViewById(R.id.tv_signUpTitle);

        tv_signUpTitle.setTypeface(Utils.mTypeface(ctx, 4));

        ll_otp.setVisibility(View.GONE);

        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        ll_countryCode.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                Intent in_login = new Intent(SignUpActivity.this, LoginActivity.class);
                in_login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in_login);
                finish();
                break;
            case R.id.tv_signup:
                String sign = tv_signup.getText().toString();
                String mobile = edt_mobile.getText().toString().trim();
                String pwd = edt_pwd.getText().toString().trim();
                String confirmPwd = edt_confirmPwd.getText().toString().trim();

                if (mobile.length() == 0) {
                    Utils.customToast(ctx, "Mobile number should not be empty.");
//                    Toast.makeText(ctx, "Mobile number should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() < 10) {
                    Utils.customToast(ctx, "Enter valid mobile number.");
//                    Toast.makeText(ctx, "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (pwd.length() == 0) {
                    Utils.customToast(ctx, "Password should not be empty.");
//                    Toast.makeText(ctx, "Password should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (country_code.equals("")) {
                    Utils.customToast(ctx, "Please select your country code.");
//                    Toast.makeText(ctx, "Password should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (calling_code.equals("")) {
                    Utils.customToast(ctx, "Please select your country code.");
//                    Toast.makeText(ctx, "Password should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (!pwd.equals(confirmPwd)) {
                    Utils.customToast(ctx, "Password does not match.");
//                    Toast.makeText(ctx, "Password does not match.", Toast.LENGTH_SHORT).show();
                } else {
                    if (sign.equals("SEND OTP")) {
                        try {
                            JSONObject js_otp = new JSONObject();
                            js_otp.put("mobile", mobile);
                            js_otp.put("calling_code", calling_code);
                            AsynTask as_otp = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SendOtpUrl), SignUpActivity.this, true);
                            as_otp.execute("" + js_otp);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (sign.equals("SIGN UP")) {
                        String otp = edt_otp.getText().toString().trim();
                        if (otp.length() == 0) {
                            Utils.customToast(ctx, "Please enter your OTP.");
//                            Toast.makeText(ctx, "Please enter your OTP.", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject js_sign = new JSONObject();
                                js_sign.put("mobile", mobile);
                                js_sign.put("password", pwd);
                                js_sign.put("password_confirmation", confirmPwd);
                                js_sign.put("otp", otp);
                                js_sign.put("location", city);
                                js_sign.put("state", state);
                                js_sign.put("country_code", country_code);
                                js_sign.put("calling_code", calling_code);
                                AsynTask as_otp = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SignUpUrl), SignUpActivity.this, true);
                                as_otp.execute("" + js_sign);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;
            case R.id.ll_countryCode:
                try {
                    AsynTask as_countryCode = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetCounteryUrl), SignUpActivity.this, false);
                    as_countryCode.execute("");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.SendOtpUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                    ll_otp.setVisibility(View.VISIBLE);
                    tv_signup.setText("SIGN UP");
                } else {
                    Utils.customToast(ctx, jobj.getString("message"));
                }
            } else {
                if (jobj.has("errors")) {
                    JSONObject jobj1 = jobj.getJSONObject("errors");
                    if (jobj1.has("mobile")) {
                        JSONArray jarr = jobj1.getJSONArray("mobile");
                        Utils.customToast(ctx, jarr.getString(0));
//                        Toast.makeText(ctx, jarr.getString(0), Toast.LENGTH_SHORT).show();
                    } else if (jobj1.has("password")) {
                        JSONArray jarr = jobj1.getJSONArray("password");
                        Utils.customToast(ctx, jarr.getString(0));
//                        Toast.makeText(ctx, jarr.getString(0), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else if (path.contains(Constants.SignUpUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent in_login = new Intent(SignUpActivity.this, LoginActivity.class);
                    in_login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(in_login);
                    finish();
                } else {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (jobj.has("errors")) {
                    JSONObject jobj1 = jobj.getJSONObject("errors");
                    if (jobj1.has("mobile")) {
                        JSONArray jarr = jobj1.getJSONArray("mobile");
                        Utils.customToast(ctx, jarr.getString(0));
//                        Toast.makeText(ctx, jarr.getString(0), Toast.LENGTH_SHORT).show();
                    } else if (jobj1.has("password")) {
                        JSONArray jarr = jobj1.getJSONArray("password");
                        Utils.customToast(ctx, jarr.getString(0));
//                        Toast.makeText(ctx, jarr.getString(0), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else if (path.contains(Constants.GetCounteryUrl)) {
            JsonParser jsonParser = new JsonParser();
            List<CountriesDto> list_country = jsonParser.mCountryCodeList(response);
            if (list_country.size() != 0) {
                new CountriesDialog(ctx, list_country, tv_country).show();
            } else {

            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            LatLng latitudeLong = new LatLng(currentLatitude, currentLongitude);
            AddressLine(latitudeLong);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void AddressLine(LatLng latLng) {

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        String addressline1 = addresses.get(0).getAddressLine(0);
        String[] add1 = addressline1.split(",");

//        area = addresses.get(0).getSubLocality();
        city = addresses.get(0).getLocality();
        state = addresses.get(0).getAdminArea();
//        country = addresses.get(0).getCountryName();
//        pinCode = addresses.get(0).getPostalCode();


    }

}
