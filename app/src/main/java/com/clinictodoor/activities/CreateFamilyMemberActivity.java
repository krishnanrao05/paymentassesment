package com.clinictodoor.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateFamilyMemberActivity extends AppCompatActivity implements View.OnClickListener, ResponseListner, DatePickerDialog.OnDateSetListener {

    EditText edt_relationship, edt_firstName, edt_lastName, edt_email, edt_mobile;
    TextView tv_dob, tv_createMember;
    Spinner spnGender, spnSalutation/*, spnAddress*/;
    Context ctx;
//    List<AddressDto> list_address = new ArrayList<>();
//    LinearLayout ll_address;
    ImageView img_menu;
    FamilyMembersDto familyMemberDto;
    String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_family_member);
        ctx = this;

        String[] gender_items = new String[]{
                "--Select Gender--",
                "Male",
                "Female",
        };

        String[] salutation_items = new String[]{
                "--Select Salutation--",
                "Ms.",
                "Mr.",
                "Mrs.",
        };

        familyMemberDto = (FamilyMembersDto) getIntent().getSerializableExtra("family_members");


        spnSalutation = findViewById(R.id.spnSalutation);
        edt_relationship = findViewById(R.id.edt_relationship);
        edt_firstName = findViewById(R.id.edt_firstName);
        edt_lastName = findViewById(R.id.edt_lastName);
        edt_email = findViewById(R.id.edt_email);
        edt_mobile = findViewById(R.id.edt_mobile);
        tv_dob = findViewById(R.id.tv_dob);
        tv_createMember = findViewById(R.id.tv_createMember);
        spnGender = findViewById(R.id.spnGender);
//        spnAddress = findViewById(R.id.spnAddress);
//        ll_address = findViewById(R.id.ll_address);
        img_menu = findViewById(R.id.img_menu);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, R.layout.spinner_item, gender_items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnGender.setAdapter(adapter);

        ArrayAdapter<String> adapter_salutation = new ArrayAdapter<String>(ctx, R.layout.spinner_item, salutation_items);
        adapter_salutation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSalutation.setAdapter(adapter_salutation);

        tv_dob.setOnClickListener(this);
        tv_createMember.setOnClickListener(this);
        img_menu.setOnClickListener(this);

        if (familyMemberDto != null) {
            edt_firstName.setText(familyMemberDto.getFirst_name());
            edt_lastName.setText(familyMemberDto.getLast_name());
            edt_mobile.setText(familyMemberDto.getMobile());
            edt_relationship.setText(familyMemberDto.getRelationship());
            tv_dob.setText(familyMemberDto.getDob());

            if (familyMemberDto.getSalutation().equals("Ms.")) {
                spnSalutation.setSelection(1);
            } else if (familyMemberDto.getSalutation().equals("Mr.")) {
                spnSalutation.setSelection(2);
            } else {
                spnSalutation.setSelection(3);
            }

            if (familyMemberDto.getGender().equals("Male")) {
                spnGender.setSelection(1);
            } else {
                spnGender.setSelection(2);
            }

            if (!familyMemberDto.getEmail().equals("null")) {
                edt_email.setText(familyMemberDto.getEmail());
            }
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        try {
//            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), CreateFamilyMemberActivity.this, false);
//            as_profile.execute("");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_createMember:
                String relationship = edt_relationship.getText().toString().trim();
                String salutation = spnSalutation.getSelectedItem().toString();
                String first_name = edt_firstName.getText().toString().trim();
                String last_name = edt_lastName.getText().toString().trim();
                String gender = spnGender.getSelectedItem().toString();
                String mobile = edt_mobile.getText().toString().trim();
                String email = edt_email.getText().toString().trim();

                if (relationship.length() == 0) {
                    Utils.customToast(ctx, "RelationShip should not be empty.");
//                    Toast.makeText(ctx, "RelationShip should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (salutation.equals("--Select Salutation--")) {
                    Utils.customToast(ctx, "Salutation should not be empty.");
//                    Toast.makeText(ctx, "Salutation should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (first_name.length() == 0) {
                    Utils.customToast(ctx, "First Name should not be empty.");
//                    Toast.makeText(ctx, "First Name should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (last_name.length() == 0) {
                    Utils.customToast(ctx, "Last Name should not be empty.");
//                    Toast.makeText(ctx, "Last Name should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (date.equals("")) {
                    Utils.customToast(ctx, "Date of Birth should not be empty.");
//                    Toast.makeText(ctx, "Date of Birth should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (gender.equals("--Select Gender--")) {
                    Utils.customToast(ctx, "Gender should not be empty.");
//                    Toast.makeText(ctx, "Gender should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() == 0) {
                    Utils.customToast(ctx, "Mobile number should not be empty.");
//                    Toast.makeText(ctx, "Mobile number should not be empty.", Toast.LENGTH_SHORT).show();
                }/* else if (list_address.size() == 0) {
                    Utils.customToast(ctx, "Please add at least one of your Addresses before adding a Family Member.");
//                    Toast.makeText(ctx, "Address should not be empty.", Toast.LENGTH_SHORT).show();
                }*/ else {
//                    String address_id = list_address.get(spnAddress.getSelectedItemPosition()).getId();
                    try {
                        JSONObject json_memb = new JSONObject();
                        json_memb.put("relationship", relationship);
                        json_memb.put("salutation", salutation);
                        json_memb.put("first_name", first_name);
                        json_memb.put("last_name", last_name);
                        json_memb.put("dob", date);
                        json_memb.put("gender", gender);
                        json_memb.put("mobile", mobile);
                        if (email.length() == 0) {
                            json_memb.put("email", null);
                        } else {
                            json_memb.put("email", email);
                        }
//                        json_memb.put("address_id", address_id);
                        AsynTask as_memb = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetFamilyMembersUrl), CreateFamilyMemberActivity.this, true);
                        as_memb.execute("" + json_memb);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.tv_dob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CreateFamilyMemberActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.img_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        /*JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetAddressUrl)) {
            list_address.clear();
            list_address = jsonParser.mAddressList(response);
            if (list_address.size() != 0) {
                ll_address.setVisibility(View.VISIBLE);
                List<String> list_addressId = new ArrayList<>();
                for (int i = 0; i < list_address.size(); i++) {
                    list_addressId.add(list_address.get(i).getAddress_type());
                }
                ArrayAdapter<String> adapter_address = new ArrayAdapter<String>(ctx, R.layout.spinner_item, list_addressId);
                adapter_address.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnAddress.setAdapter(adapter_address);
            } else {
                ll_address.setVisibility(View.GONE);
            }
        } else */if (path.contains(Constants.GetFamilyMembersUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = "";
        String month = "", day = "";
        int mnth = ++monthOfYear;
        if (mnth < 10) {
            month = "0" + mnth;
        } else {
            month = "" + mnth;
        }
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }
        date = year + "-" + month + "-" + day;
        tv_dob.setText(day + "-" + month + "-" + year);

    }
}
