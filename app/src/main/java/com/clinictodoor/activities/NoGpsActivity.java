package com.clinictodoor.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.dialogs.CitiesDialog;
import com.clinictodoor.utils.Utils;

public class NoGpsActivity extends AppCompatActivity {

    Button btn_selectLocations;
    TextView tv_text;
    LinearLayout view_transparent;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_gps);
        ctx = this;

        btn_selectLocations = findViewById(R.id.btn_selectLocations);
        tv_text = findViewById(R.id.tv_text);
        view_transparent = findViewById(R.id.view_transparent);


        btn_selectLocations.setTypeface(Utils.mTypeface(ctx, 4));
        tv_text.setTypeface(Utils.mTypeface(ctx, 3));


        btn_selectLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CitiesDialog(ctx, "gps").show();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
