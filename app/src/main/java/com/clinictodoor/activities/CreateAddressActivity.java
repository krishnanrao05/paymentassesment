package com.clinictodoor.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

public class CreateAddressActivity extends AppCompatActivity implements ResponseListner, View.OnClickListener {

    Context ctx;
    EditText edt_addressType, edt_doorNo, edt_street, edt_landmark, edt_city, edt_state, edt_country, edt_pinCode;
    TextView tv_saveAddress;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address);
        ctx = this;

        edt_addressType = findViewById(R.id.edt_addressType);
        edt_doorNo = findViewById(R.id.edt_doorNo);
        edt_street = findViewById(R.id.edt_street);
        edt_landmark = findViewById(R.id.edt_landmark);
        edt_city = findViewById(R.id.edt_city);
        edt_state = findViewById(R.id.edt_state);
        edt_country = findViewById(R.id.edt_country);
        edt_pinCode = findViewById(R.id.edt_pinCode);
        tv_saveAddress = findViewById(R.id.tv_saveAddress);
        img_back = findViewById(R.id.img_back);

        AddressDto address_dto = (AddressDto) getIntent().getSerializableExtra("address_dto");
        if (address_dto != null) {
            edt_addressType.setText(address_dto.getAddress_type());
            edt_doorNo.setText(address_dto.getDoor_no());
            edt_street.setText(address_dto.getStreet());
            edt_landmark.setText(address_dto.getLandmark());
            edt_city.setText(address_dto.getCity());
            edt_state.setText(address_dto.getStreet());
            edt_country.setText(address_dto.getCountry());
            edt_pinCode.setText(address_dto.getPin_code());
            tv_saveAddress.setText("Update Address");
        }


        tv_saveAddress.setOnClickListener(this);
        img_back.setOnClickListener(this);

        tv_saveAddress.setTypeface(Utils.mTypeface(ctx, 4));

    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.GetAddressUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Utils.customToast(ctx, jobj.getString("message"));
//                Toast.makeText(ctx, response, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_saveAddress:
                String address_type = edt_addressType.getText().toString().trim();
                String door_no = edt_doorNo.getText().toString().trim();
                String street = edt_street.getText().toString().trim();
                String landmark = edt_landmark.getText().toString().trim();
                String city = edt_city.getText().toString().trim();
                String state = edt_state.getText().toString().trim();
                String country = edt_country.getText().toString().trim();
                String pin_code = edt_pinCode.getText().toString().trim();

                if (address_type.length() == 0) {
                    Utils.customToast(ctx, "Please enter Address Type.");
//                    Toast.makeText(ctx, "Please enter Address Type.", Toast.LENGTH_SHORT).show();
                } else if (door_no.length() == 0) {
                    Utils.customToast(ctx, "Please enter Door Number.");
//                    Toast.makeText(ctx, "Please enter Door Number.", Toast.LENGTH_SHORT).show();
                } else if (street.length() == 0) {
                    Utils.customToast(ctx, "Please enter Street Name.");
//                    Toast.makeText(ctx, "Please enter Street Name.", Toast.LENGTH_SHORT).show();
                } else if (landmark.length() == 0) {
                    Utils.customToast(ctx, "Please enter Landmark Name.");
//                    Toast.makeText(ctx, "Please enter Landmark Name.", Toast.LENGTH_SHORT).show();
                } else if (city.length() == 0) {
                    Utils.customToast(ctx, "Please enter City Name.");
//                    Toast.makeText(ctx, "Please enter City Name.", Toast.LENGTH_SHORT).show();
                } else if (state.length() == 0) {
                    Utils.customToast(ctx, "Please enter State Name.");
//                    Toast.makeText(ctx, "Please enter State Name.", Toast.LENGTH_SHORT).show();
                } else if (country.length() == 0) {
                    Utils.customToast(ctx, "Please enter Country Name.");
//                    Toast.makeText(ctx, "Please enter Country Name.", Toast.LENGTH_SHORT).show();
                } else if (pin_code.length() == 0) {
                    Utils.customToast(ctx, "Please enter Pin Code Number.");
//                    Toast.makeText(ctx, "Please enter Pin Code Number.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject json_address = new JSONObject();
                        json_address.put("address_type", address_type);
                        json_address.put("door_no", door_no);
                        json_address.put("street", street);
                        json_address.put("landmark", landmark);
                        json_address.put("city", city);
                        json_address.put("state", state);
                        json_address.put("country", country);
                        json_address.put("pin_code", pin_code);
                        AsynTask as_add = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), CreateAddressActivity.this, true);
                        as_add.execute("" + json_address);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }
}
