package com.clinictodoor.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.UserDetailsDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ResponseListner {

    TextView tv_login, tv_forget, tv_signUp, tv_loginTitle;
    Context ctx;
    EditText edt_mobile, edt_pwd;
    UserDetailsDto userDetailsDto = new UserDetailsDto();
    ImageView img_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ctx = this;

        tv_login = findViewById(R.id.tv_login);
        tv_forget = findViewById(R.id.tv_forget);
        tv_signUp = findViewById(R.id.tv_signUp);
        edt_mobile = findViewById(R.id.edt_mobile);
        edt_pwd = findViewById(R.id.edt_pwd);
        img_logo = findViewById(R.id.img_logo);
        tv_loginTitle = findViewById(R.id.tv_loginTitle);

        tv_signUp.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        img_logo.setOnClickListener(this);

        tv_signUp.setTypeface(Utils.mTypeface(ctx, 4));
        tv_forget.setTypeface(Utils.mTypeface(ctx, 4));
        tv_login.setTypeface(Utils.mTypeface(ctx, 1));
        tv_loginTitle.setTypeface(Utils.mTypeface(ctx, 4));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                String mobile = edt_mobile.getText().toString().trim();
                String pwd = edt_pwd.getText().toString().trim();
                if (mobile.length() == 0) {
                    Utils.customToast(ctx, "Mobile number should not be empty.");
//                    Toast.makeText(ctx, "Mobile number should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() < 10) {
                    Utils.customToast(ctx, "Please enter a valid mobile number.");
//                    Toast.makeText(ctx, "Please enter a valid mobile number.", Toast.LENGTH_SHORT).show();
                } else if (pwd.length() == 0) {
                    Utils.customToast(ctx, "Password should not be empty.");
//                    Toast.makeText(ctx, "Password should not be empty.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject js_login = new JSONObject();
                        js_login.put("username", mobile);
                        js_login.put("password", pwd);
                        AsynTask as_login = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.LoginUrl), LoginActivity.this, true);
                        as_login.execute("" + js_login);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.tv_signUp:
                if (Utils.isGPSEnabled(ctx)) {
                    Intent in_sign = new Intent(LoginActivity.this, SignUpActivity.class);
                    startActivity(in_sign);
                } else {
                    Utils.customToast(ctx, "Please enable GPS...");
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
//                    Toast.makeText(ctx, "Please enable GPS...", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_logo:
                Intent in_hom = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(in_hom);
                break;
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.LoginUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    JsonParser jsonParser = new JsonParser();
                    userDetailsDto = jsonParser.mUserDetails(response);
                    SharedPreferences sharedpreferences = getSharedPreferences("login_access", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("access_token", jobj.getString("access_token"));
                    editor.putString("refresh_token", jobj.getString("refresh_token"));
                    if (userDetailsDto.getProfileDto() != null) {
                        editor.putString("profile_date", "yes");
                    } else {
                        editor.putString("profile_date", "no");
                    }
                    editor.putString("mobile", userDetailsDto.getUsername());
                    editor.commit();
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent in_home = new Intent(LoginActivity.this, HomeActivity.class);
                    in_home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(in_home);
                    finish();
                } else {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
