package com.clinictodoor.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;
import com.clinictodoor.adapters.CartAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.database.DBHelper;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.fragments.FamilyMembersFragment;
import com.clinictodoor.fragments.MyAddressesFragment;
import com.clinictodoor.fragments.RazorPayFragment;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity implements View.OnClickListener, ResponseListner {

    RecyclerView rec_cartList;
    Context ctx;
    CartAdapter cartAdapter;
    Button btn_payNow;

    JSONArray cartDetails = new JSONArray();
    TextView totalAmount, tv_totalAmount;
    ImageView img_back;
    DBHelper db;
    List<FamilyMembersDto> list_family = new ArrayList<>();
    List<AddressDto> list_address = new ArrayList<>();
    List<CartDto> list_cart = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ctx = this;

        db = new DBHelper(ctx);
        list_cart = db.getCartList();

        btn_payNow = findViewById(R.id.btn_payNow);
        totalAmount = findViewById(R.id.totalAmount);
        tv_totalAmount = findViewById(R.id.tv_totalAmount);
        img_back = findViewById(R.id.img_back);
        rec_cartList = findViewById(R.id.rec_cartList);
        rec_cartList.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));


        btn_payNow.setTypeface(Utils.mTypeface(ctx, 4));
        totalAmount.setTypeface(Utils.mTypeface(ctx, 3));
        tv_totalAmount.setTypeface(Utils.mTypeface(ctx, 4));

        try {
            AsynTask as_family = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetFamilyMembersUrl), CartActivity.this, false);
            as_family.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyCartPrice();

        img_back.setOnClickListener(this);
        btn_payNow.setOnClickListener(this);

    }

    public void notifyCartPrice() {
        List<CartDto> list_cart = db.getCartList();
        if (list_cart.size() != 0) {
            int value = 0;
            for (int i = 0; i < list_cart.size(); i++) {
                try {
                    JSONObject productObject = new JSONObject();
                    productObject.put("service_id",list_cart.get(i).getService_id());
                    productObject.put("member_id",list_cart.get(i).getId());
                    productObject.put("package_id",list_cart.get(i).getPackage_type());
                    productObject.put("test_id",list_cart.get(i).getTest_id());
                    productObject.put("for_whom",list_cart.get(i).getFamily_name());
                    productObject.put("address_id",list_cart.get(i).getService_address_id());
                    productObject.put("date",list_cart.get(i).getService_date());
                    productObject.put("time",list_cart.get(i).getService_time());

                    cartDetails.put(productObject);

                }catch (JSONException e)                {
                    e.printStackTrace();
                }

                value = value + Integer.parseInt(list_cart.get(i).getService_image());

            }
            tv_totalAmount.setText(getResources().getString(R.string.Price) + " " + value);
        } else {
            tv_totalAmount.setText(getResources().getString(R.string.Price) + " 0");
        }
    }

    @Override
    public void onClick(View view) {
        Log.e("onclick", "onClick: view : "+view.getId());
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_payNow:
                Toast.makeText(ctx, "PayNow", Toast.LENGTH_SHORT).show();
                loadRazorPayFragment();
                Log.e("Hai", "onClick: PayNow");
                break;
        }
    }

    public void loadRazorPayFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment razorPayFragment = new RazorPayFragment();
        Bundle bundle = new Bundle();
        bundle.putString("cartData", String.valueOf(cartDetails));
        razorPayFragment.setArguments(bundle);
        transaction.replace(R.id.razorPayLayout,razorPayFragment);
        transaction.commit();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetFamilyMembersUrl)) {
            list_family.clear();
            list_family = jsonParser.mFamilyMembList(response);
            try {
                AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), CartActivity.this, false);
                as_profile.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (path.contains(Constants.GetAddressUrl)) {
            list_address.clear();
            list_address = jsonParser.mAddressList(response);

            if (list_cart.size() != 0) {
                cartAdapter = new CartAdapter(ctx, list_cart, list_family, list_address);
                rec_cartList.setAdapter(cartAdapter);
            } else {

            }
        }
    }

}
