package com.clinictodoor.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.dtos.UserDetailsDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.RoundedCornersTransform;
import com.clinictodoor.utils.Utils;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.Calendar;

public class CreateProfileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, ResponseListner {

    EditText edt_firstName, edt_lastName, edt_email;
    TextView tv_dob, tv_saveProfile;
    Spinner spnGender, spnSalutation;
    Context ctx;
    ImageView img_menu;
    ProfileDto profile_dto;
    ImageView img_profilePic;
    String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_porfile);
        ctx = this;

        profile_dto = (ProfileDto) getIntent().getSerializableExtra("profile");

        String[] gender_items = new String[]{
                "--Select Gender--",
                "Male",
                "Female",
        };

        String[] salutation_items = new String[]{
                "--Select Salutation--",
                "Ms.",
                "Mr.",
                "Mrs.",
        };

        spnSalutation = findViewById(R.id.spnSalutation);
        edt_firstName = findViewById(R.id.edt_firstName);
        edt_lastName = findViewById(R.id.edt_lastName);
        edt_email = findViewById(R.id.edt_email);
        tv_dob = findViewById(R.id.tv_dob);
        tv_saveProfile = findViewById(R.id.tv_saveProfile);
        spnGender = findViewById(R.id.spnGender);
        img_menu = findViewById(R.id.img_menu);
        img_profilePic = findViewById(R.id.img_profilePic);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, R.layout.spinner_item, gender_items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnGender.setAdapter(adapter);

        ArrayAdapter<String> adapter_salutation = new ArrayAdapter<String>(ctx, R.layout.spinner_item, salutation_items);
        adapter_salutation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSalutation.setAdapter(adapter_salutation);

        tv_dob.setOnClickListener(this);
        tv_saveProfile.setOnClickListener(this);
        img_menu.setOnClickListener(this);

        if (profile_dto != null) {
            if (profile_dto.getFirst_name() != null)
                edt_firstName.setText(profile_dto.getFirst_name());
            if (profile_dto.getLast_name() != null)
                edt_lastName.setText(profile_dto.getLast_name());
            if (profile_dto.getEmail() != null)
                edt_email.setText(profile_dto.getEmail());
            if (profile_dto.getDob() != null)
                tv_dob.setText(profile_dto.getDob());
            if (profile_dto.getGender() != null)
                if (profile_dto.getGender().equals("Male")) {
                    spnGender.setSelection(1);
                } else if (profile_dto.getGender().equals("Female")) {
                    spnGender.setSelection(2);
                }
            if (profile_dto.getSalutation() != null)
                if (profile_dto.getSalutation().equals("Mr.")) {
                    spnSalutation.setSelection(1);
                } else {
                    spnSalutation.setSelection(2);
                }

            Picasso.with(ctx).load(Constants.ImageBaseUrl + profile_dto.getPic())
                    .placeholder(R.drawable.user_profile)
                    .transform(new RoundedCornersTransform())
                    .error(R.drawable.user_profile).into(img_profilePic);

        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = "";
        String month = "", day = "";
        int mnth = ++monthOfYear;
        if (mnth < 10) {
            month = "0" + (mnth);
        } else {
            month = "" + (mnth);
        }
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }
        date = year + "-" + month + "-" + day;
        tv_dob.setText(day + "-" + month + "-" + year);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_dob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CreateProfileActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.tv_saveProfile:
                String salutation = spnSalutation.getSelectedItem().toString();
                String first_name = edt_firstName.getText().toString().trim();
                String last_name = edt_lastName.getText().toString().trim();
                String gender = spnGender.getSelectedItem().toString();
                String email = edt_email.getText().toString().trim();

                if (salutation.equals("--Select Salutation--")) {
                    Utils.customToast(ctx, "Salutation should not be empty.");
//                    Toast.makeText(ctx, "Salutation should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (first_name.length() == 0) {
                    Utils.customToast(ctx, "First name should not be empty.");
//                    Toast.makeText(ctx, "First name should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (last_name.length() == 0) {
                    Utils.customToast(ctx, "Last name should not be empty.");
//                    Toast.makeText(ctx, "Last name should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (date.equals("")) {
                    Utils.customToast(ctx, "Date of Birth should not be empty.");
//                    Toast.makeText(ctx, "Date of Birth should not be empty.", Toast.LENGTH_SHORT).show();
                } else if (gender.equals("--Select Gender--")) {
                    Utils.customToast(ctx, "Please select gender.");
//                    Toast.makeText(ctx, "Please select gender.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject j_profile = new JSONObject();
                        j_profile.put("salutation", salutation);
                        j_profile.put("first_name", first_name);
                        j_profile.put("last_name", last_name);
                        j_profile.put("email", email);
                        j_profile.put("dob", date);
                        j_profile.put("gender", gender);
                        AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), CreateProfileActivity.this, true);
                        as_profile.execute("" + j_profile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.img_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.SaveProfileUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    JsonParser jsonParser = new JsonParser();
                    UserDetailsDto userDetailsDto = jsonParser.mUserDetails(response);
                    Utils.customToast(ctx, jobj.getString("message"));
                    SharedPreferences sharedpreferences = getSharedPreferences("login_access", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("profile_date", "yes");
                    editor.commit();
                    finish();
                } else {
                    Utils.customToast(ctx, jobj.getString("message"));
//                    Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
