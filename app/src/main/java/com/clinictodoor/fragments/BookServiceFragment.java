package com.clinictodoor.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.BookServicesAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;

import org.json.JSONException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookServiceFragment extends Fragment implements ResponseListner {

    Context ctx;
    RecyclerView rec_bookServices;
    BookServicesAdapter bookServicesAdapter;
    ProfileDto profile_dto;
    List<AddressDto> list_address = new ArrayList<>();

    public BookServiceFragment() {
        // Required empty public constructor
    }

    public static BookServiceFragment newInstance(String s) {
        BookServiceFragment fragment = new BookServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_book_service, container, false);
        ctx = this.getActivity();

        ((HomeActivity) ctx).bookAServiceItems();

        rec_bookServices = rootView.findViewById(R.id.rec_bookServices);
        rec_bookServices.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();


        try {
            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), BookServiceFragment.this, false);
            as_profile.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), BookServiceFragment.this, false);
            as_profile.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }


        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("location", Context.MODE_PRIVATE);
        String area = sharedPrefgcm.getString("city", "");

        if (area.equals("")) {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl), BookServiceFragment.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl + "/" + area), BookServiceFragment.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.SaveProfileUrl)) {
            profile_dto = jsonParser.mUserProfile(response);
        } else if (path.contains(Constants.GetServicesUrl)) {
            List<ServicesDto> list_services = jsonParser.mServicesList(response);
            if (list_services.size() != 0) {
                ((HomeActivity) ctx).hideTransparentBar(2);
                for (int i = 0; i < list_services.size(); i++) {
                    if (i == 0) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac);
                    } else if (i == 1) {
                        list_services.get(i).setIcon(R.drawable.ic_illlness_prevention);
                    } else if (i == 2) {
                        list_services.get(i).setIcon(R.drawable.ic_workplace_wellness);
                    } else if (i == 3) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac_screening_live);
                    } else if (i == 4) {
                        list_services.get(i).setIcon(R.drawable.ic_breast_cancer);
                    } else if (i == 5) {
                        list_services.get(i).setIcon(R.drawable.ic_lab_tests);
                    }
                }
                bookServicesAdapter = new BookServicesAdapter(ctx, list_services, profile_dto, list_address);
                rec_bookServices.setAdapter(bookServicesAdapter);
            } else {
                ((HomeActivity) ctx).hideTransparentBar(1);
            }
        } else if (path.contains(Constants.GetAddressUrl)) {
            list_address = jsonParser.mAddressList(response);

        }
    }
}
