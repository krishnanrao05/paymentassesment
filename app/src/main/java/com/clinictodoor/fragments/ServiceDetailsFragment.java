package com.clinictodoor.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceDetailsFragment extends Fragment implements ResponseListner {


    ImageView img_service;
    TextView tv_serviceName, tv_bookService, tv_price, tv_desc_1, desc_2, tv_teaser;
    Context ctx;
    String service_id;
    int icon;

    public ServiceDetailsFragment() {
        // Required empty public constructor
    }

    public static ServiceDetailsFragment newInstance(String s) {
        ServiceDetailsFragment fragment = new ServiceDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_service_details, container, false);
        ctx = this.getActivity();


        service_id = getArguments().getString("service_id");
        icon = getArguments().getInt("icon");

        img_service = rootView.findViewById(R.id.img_service);
        tv_serviceName = rootView.findViewById(R.id.tv_serviceName);
        tv_bookService = rootView.findViewById(R.id.tv_bookService);
        tv_price = rootView.findViewById(R.id.tv_price);
        tv_desc_1 = rootView.findViewById(R.id.tv_desc_1);
        desc_2 = rootView.findViewById(R.id.desc_2);
        tv_teaser = rootView.findViewById(R.id.tv_teaser);


        tv_serviceName.setTypeface(Utils.mTypeface(ctx, 4));
        tv_bookService.setTypeface(Utils.mTypeface(ctx, 4));
        tv_price.setTypeface(Utils.mTypeface(ctx, 4));
        tv_desc_1.setTypeface(Utils.mTypeface(ctx, 3));
        desc_2.setTypeface(Utils.mTypeface(ctx, 3));
        tv_teaser.setTypeface(Utils.mTypeface(ctx, 3));


        try {
            AsynTask as_serviceDetails = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl + "/" + service_id + "/all"), ServiceDetailsFragment.this, false);
            as_serviceDetails.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.GetServicesUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("service")) {
                JSONObject json = jobj.getJSONObject("service");
                if (json.has("title")) {
                    tv_serviceName.setText(json.getString("title"));

                    if (json.getString("title").equals("Workplace Wellness")) {
                        tv_price.setText("+91 844 844 8630");
                        tv_bookService.setVisibility(View.GONE);
                    } else if (json.getString("title").equals("Lab Tests At Home")) {
                        tv_price.setText("Price Varies");
                    } else {
                        if (json.has("price")) {
                            tv_price.setText(ctx.getResources().getString(R.string.Price) + " " + json.getString("price"));
                        }
                    }
                }
                if (json.has("description_long")) {
                    JSONArray jarr = json.getJSONArray("description_long");
                    if (jarr.length() <= 2) {
                        tv_desc_1.setText(jarr.getString(0));
                        desc_2.setText(jarr.getString(1));
                    } else {
                        tv_desc_1.setText(jarr.getString(0));
                        desc_2.setVisibility(View.GONE);
                    }
                }

                if (json.has("teaser")) {
                    tv_teaser.setText(json.getString("teaser"));
                }

                img_service.setImageResource(icon);
            }
        }
    }
}
