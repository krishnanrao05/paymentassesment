package com.clinictodoor.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateAddressActivity;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.AddressAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;

import java.net.URL;
import java.util.List;

public class MyAddressesFragment extends Fragment implements ResponseListner, View.OnClickListener {

    Context ctx;
    RecyclerView rec_address;
    AddressAdapter addressAdapter;
    TextView tv_emptyAddress;
    FloatingActionButton fab_add;

    public static MyAddressesFragment newInstance(String s) {
        MyAddressesFragment fragment = new MyAddressesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_address, container, false);
        ctx = this.getActivity();
        ((HomeActivity) ctx).myAddressesBtnClick();

        fab_add = rootView.findViewById(R.id.fab_add);
        tv_emptyAddress = rootView.findViewById(R.id.tv_emptyAddress);
        rec_address = rootView.findViewById(R.id.rec_address);
        rec_address.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        tv_emptyAddress.setTypeface(Utils.mTypeface(ctx, 3));

        fab_add.setOnClickListener(this);

        rec_address.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab_add.getVisibility() == View.VISIBLE) {
                    fab_add.hide();
                } else if (dy < 0 && fab_add.getVisibility() != View.VISIBLE) {
                    fab_add.show();
                }
            }
        });

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), MyAddressesFragment.this, false);
            as_profile.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetAddressUrl)) {
            List<AddressDto> list_address = jsonParser.mAddressList(response);
            if (list_address.size() != 0) {
                addressAdapter = new AddressAdapter(ctx, list_address);
                rec_address.setAdapter(addressAdapter);
                tv_emptyAddress.setVisibility(View.GONE);
                rec_address.setVisibility(View.VISIBLE);
            } else {
                tv_emptyAddress.setVisibility(View.VISIBLE);
                rec_address.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                Intent in_addCrate = new Intent(ctx, CreateAddressActivity.class);
                startActivity(in_addCrate);
                break;
        }
    }
}
