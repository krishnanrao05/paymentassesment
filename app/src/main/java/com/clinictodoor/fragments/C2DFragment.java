package com.clinictodoor.fragments;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.BannersViewPagerAdapter;
import com.clinictodoor.adapters.ServicesAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;

import org.json.JSONException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * A simple {@link Fragment} subclass.
 */
public class C2DFragment extends Fragment implements ResponseListner {

    ViewPager viewPagerBanner;
    CircleIndicator circular_indicator;
    BannersViewPagerAdapter bannersViewPagerAdapter;
    Context ctx;
    List<Integer> list_images = new ArrayList<>();
    RecyclerView rec_services;
    ServicesAdapter servicesAdapter;


    BroadcastReceiver br;
    AlarmManager am;
    PendingIntent pi;


    public C2DFragment() {
        // Required empty public constructor
    }

    public static C2DFragment newInstance(String s) {
        C2DFragment fragment = new C2DFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_c2_d, container, false);
        ctx = this.getActivity();
        ((HomeActivity) ctx).homeBtnClick();
        ((HomeActivity) ctx).reportsItems();
        list_images.clear();
        list_images.add(R.drawable.image_4);
        list_images.add(R.drawable.image_1);
        list_images.add(R.drawable.image_2);
        list_images.add(R.drawable.image_3);

        rec_services = rootView.findViewById(R.id.rec_services);
        viewPagerBanner = rootView.findViewById(R.id.viewPagerBanner);
        circular_indicator = rootView.findViewById(R.id.circular_indicator);


        bannersViewPagerAdapter = new BannersViewPagerAdapter(ctx, list_images);
        viewPagerBanner.setAdapter(bannersViewPagerAdapter);
        circular_indicator.setViewPager(viewPagerBanner);

        rec_services.setLayoutManager(new GridLayoutManager(ctx, 2));




        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("location", Context.MODE_PRIVATE);
        String area = sharedPrefgcm.getString("city", "");

        if (area.equals("")) {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl), C2DFragment.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                AsynTask as_services = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetServicesUrl + "/" + area), C2DFragment.this, false);
                as_services.execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetServicesUrl)) {
            List<ServicesDto> list_services = jsonParser.mServicesList(response);

            if (list_services.size() != 0) {
                ((HomeActivity) ctx).hideTransparentBar(2);
                for (int i = 0; i < list_services.size(); i++) {
                    if (i == 0) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac);
                    } else if (i == 1) {
                        list_services.get(i).setIcon(R.drawable.ic_illlness_prevention);
                    } else if (i == 2) {
                        list_services.get(i).setIcon(R.drawable.ic_workplace_wellness);
                    } else if (i == 3) {
                        list_services.get(i).setIcon(R.drawable.ic_cardiac_screening_live);
                    } else if (i == 4) {
                        list_services.get(i).setIcon(R.drawable.ic_breast_cancer);
                    } else if (i == 5) {
                        list_services.get(i).setIcon(R.drawable.ic_lab_tests);
                    }
                }
                servicesAdapter = new ServicesAdapter(ctx, list_services);
                rec_services.setAdapter(servicesAdapter);
            } else {
                ((HomeActivity) ctx).hideTransparentBar(1);
            }
        }
    }
}
