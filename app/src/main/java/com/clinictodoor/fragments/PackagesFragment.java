package com.clinictodoor.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clinictodoor.R;
import com.clinictodoor.adapters.PackagesAdapter;
import com.clinictodoor.dialogs.AddToCartDialog;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;


public class PackagesFragment extends Fragment {

    RecyclerView rec_packages;
    PackagesAdapter packagesAdapter;
    Context ctx;
    List<PackagesDto> list_packages = new ArrayList<>();
    List<AddressDto> addresses_list = new ArrayList<>();

    public PackagesFragment() {
    }

    public static PackagesFragment newInstance(String param1) {
        PackagesFragment fragment = new PackagesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_packages, container, false);
        ctx = this.getActivity();

        list_packages = (List<PackagesDto>) getArguments().getSerializable("packages_list");
        addresses_list = (List<AddressDto>) getArguments().getSerializable("addresses_list");
        String hint = getArguments().getString("hint");

        rec_packages = rootView.findViewById(R.id.rec_packages);
        rec_packages.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        if (hint.equals("packages")) {
            if (list_packages.size() != 0) {
                for (int i = 0; i < list_packages.size(); i++) {
                    if (i == 0) {
                        list_packages.get(i).setIcon(R.drawable.packages_1);
                    } else if (i == 1) {
                        list_packages.get(i).setIcon(R.drawable.packages_2);
                    } else if (i == 2) {
                        list_packages.get(i).setIcon(R.drawable.packages_3);
                    } else if (i == 3) {
                        list_packages.get(i).setIcon(R.drawable.packages_4);
                    } else if (i == 4) {
                        list_packages.get(i).setIcon(R.drawable.packages_5);
                    } else if (i == 5) {
                        list_packages.get(i).setIcon(R.drawable.packages_6);
                    } else if (i == 6) {
                        list_packages.get(i).setIcon(R.drawable.packages_7);
                    }
                }
            }
        }

        packagesAdapter = new PackagesAdapter(ctx, list_packages, addresses_list);
        rec_packages.setAdapter(packagesAdapter);


        return rootView;
    }
}
