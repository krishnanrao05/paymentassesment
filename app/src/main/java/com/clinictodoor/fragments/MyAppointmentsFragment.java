package com.clinictodoor.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.MyAppointmentsAdapter;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;

public class MyAppointmentsFragment extends Fragment implements ResponseListner, View.OnClickListener {

    RecyclerView rec_myAppointments;
    MyAppointmentsAdapter appointmentsAdapter;
    Context ctx;
    TextView tv_present, tv_past;

    public MyAppointmentsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyAppointmentsFragment newInstance(String param1) {
        MyAppointmentsFragment fragment = new MyAppointmentsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_appointments, container, false);
        ctx = this.getActivity();

        ((HomeActivity) ctx).myAppointmentsItems();
        ((HomeActivity) ctx).myAppointmentsBtnClick();


        tv_present = rootView.findViewById(R.id.tv_present);
        tv_past = rootView.findViewById(R.id.tv_past);
        rec_myAppointments = rootView.findViewById(R.id.rec_myAppointments);
        rec_myAppointments.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        appointmentsAdapter = new MyAppointmentsAdapter(ctx);
        rec_myAppointments.setAdapter(appointmentsAdapter);


        tv_present.setOnClickListener(this);
        tv_past.setOnClickListener(this);


        tv_present.setTypeface(Utils.mTypeface(ctx, 4));
        tv_past.setTypeface(Utils.mTypeface(ctx, 4));
        return rootView;
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_present:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    tv_present.setBackground(ctx.getResources().getDrawable(R.drawable.present_view_bg));
                    tv_past.setBackgroundColor(Color.parseColor("#00000000"));
                    tv_present.setTextColor(ctx.getResources().getColor(R.color.white_color));
                    tv_past.setTextColor(ctx.getResources().getColor(R.color.white_color));
                }
                break;
            case R.id.tv_past:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    tv_present.setBackgroundColor(Color.parseColor("#00000000"));
                    tv_past.setBackground(ctx.getResources().getDrawable(R.drawable.past_view_bg));
                    tv_present.setTextColor(ctx.getResources().getColor(R.color.white_color));
                    tv_past.setTextColor(ctx.getResources().getColor(R.color.white_color));
                }
                break;
        }
    }
}
