package com.clinictodoor.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    Context ctx;

    public AboutFragment() {
        // Required empty public constructor
    }


    public static AboutFragment newInstance(String s) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ctx = this.getActivity();
        ((HomeActivity) ctx).aboutBtnClick();
        return rootView;
    }

}
