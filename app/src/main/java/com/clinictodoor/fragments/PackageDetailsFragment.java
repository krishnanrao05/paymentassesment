package com.clinictodoor.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;


public class PackageDetailsFragment extends Fragment implements ResponseListner {


    ImageView img_service;
    TextView tv_serviceName, tv_bookService, tv_price, tv_desc;
    Context ctx;
    String package_id;
    int icon;

    public PackageDetailsFragment() {
        // Required empty public constructor
    }

    public static PackageDetailsFragment newInstance(String s) {
        PackageDetailsFragment fragment = new PackageDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_package_details, container, false);
        ctx = this.getActivity();

        package_id = getArguments().getString("package_id");
        icon = getArguments().getInt("icon");

        img_service = rootView.findViewById(R.id.img_service);
        tv_serviceName = rootView.findViewById(R.id.tv_serviceName);
        tv_bookService = rootView.findViewById(R.id.tv_bookService);
        tv_price = rootView.findViewById(R.id.tv_price);
        tv_desc = rootView.findViewById(R.id.tv_desc);

        tv_serviceName.setTypeface(Utils.mTypeface(ctx, 4));
        tv_bookService.setTypeface(Utils.mTypeface(ctx, 4));
        tv_price.setTypeface(Utils.mTypeface(ctx, 4));
        tv_desc.setTypeface(Utils.mTypeface(ctx, 3));


        try {
            AsynTask as_packageDetails = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetPackagesUrl + "/" + package_id), PackageDetailsFragment.this, false);
            as_packageDetails.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.GetPackagesUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("package")) {
                JSONObject json = jobj.getJSONObject("package");
                if (json.has("title")) {
                    tv_serviceName.setText(json.getString("title"));
                }
                if (json.has("price")) {
                    tv_price.setText(ctx.getResources().getString(R.string.Price) + " " + json.getString("price"));
                }
                if (json.has("description")) {
                    tv_desc.setText(Html.fromHtml(json.getString("description")));
                }
                img_service.setImageResource(icon);
            }
        }
    }
}
