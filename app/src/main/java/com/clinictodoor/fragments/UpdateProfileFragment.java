package com.clinictodoor.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateProfileActivity;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.asynctask.UploadImage;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.RoundedCornersTransform;
import com.clinictodoor.utils.Utils;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.util.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

public class UpdateProfileFragment extends Fragment implements View.OnClickListener, ResponseListner {


    Context ctx;
    TextView tv_updateProfile, tv_createProfile;
    ImageView img_profilePic;
    String selectedPath, profile_pic;
    private int SELECT_FILE = 1;

    public UpdateProfileFragment() {
        // Required empty public constructor
    }


    public static UpdateProfileFragment newInstance(String s) {
        UpdateProfileFragment fragment = new UpdateProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_update_profile_pic, container, false);
        ctx = this.getActivity();

        tv_updateProfile = rootView.findViewById(R.id.tv_updateProfile);
        img_profilePic = rootView.findViewById(R.id.img_profilePic);
        tv_createProfile = rootView.findViewById(R.id.tv_createProfile);

        tv_updateProfile.setTypeface(Utils.mTypeface(ctx, 4));
        tv_createProfile.setTypeface(Utils.mTypeface(ctx, 4));


        img_profilePic.setOnClickListener(this);
        tv_createProfile.setOnClickListener(this);
        return rootView;
    }

    private void uploadVideo() {
        class UploadVideo extends AsyncTask<Void, Void, String> {

//            private ProgressDialog progressBar;
//            private boolean show_progress = true;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
//                ProgressDialog progressDialog = new ProgressDialog(ctx);
//                progressDialog.setTitle("Uploading Image");
//                progressDialog.setMessage("Please wait...");
//                progressDialog.setCancelable(false);
//                progressBar = progressDialog;
//                progressBar.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
//                if (progressBar != null || progressBar.isShowing()) {
//                    progressBar.dismiss();
//                }
                try {
                    JSONObject jobj = new JSONObject(s);
                    if (jobj.has("success")) {
                        if (jobj.getBoolean("success")) {
                            Utils.customToast(ctx, jobj.getString("message"));
//                            Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                            try {
                                AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), UpdateProfileFragment.this, false);
                                as_profile.execute("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Utils.customToast(ctx, jobj.getString("message"));
//                            Toast.makeText(ctx, jobj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                UploadImage u = new UploadImage();
                String msg = u.uploadVideo(selectedPath, ctx);
                return msg;
            }
        }
        UploadVideo uv = new UploadVideo();
        uv.execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), UpdateProfileFragment.this, false);
            as_profile.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_profilePic:
//                if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA)
//                        == PackageManager.PERMISSION_DENIED) {
//                    ActivityCompat.requestPermissions(UpdateProfileFragment.this, new String[]{Manifest.permission.CAMERA}, 100);
//                } else {
                galleryIntent();
//                }
                break;
            case R.id.tv_createProfile:
                Intent in_createProfile = new Intent(ctx, CreateProfileActivity.class);
                startActivity(in_createProfile);
                break;
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                selectedPath = FileUtils.getPath(ctx, data.getData());
                uploadVideo();
            }
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JSONObject jobj = new JSONObject(response);
        if (path.contains(Constants.SaveProfileUrl)) {
            JsonParser jsonParser = new JsonParser();
            if (jobj.has("success")) {
                if (jobj.getBoolean("success")) {
                    if (jobj.has("user_profile")) {
                        if (!jobj.getString("user_profile").equals("null")) {
                            ProfileDto profile_dto = jsonParser.mUserProfile(response);
                            Picasso.with(ctx).load(Constants.ImageBaseUrl + profile_dto.getPic())
                                    .placeholder(R.drawable.profile_pic_bg)
                                     .transform(new RoundedCornersTransform())
                                    .error(R.drawable.profile_pic_bg).into(img_profilePic);
                        } else {
                            Picasso.with(ctx).load(Constants.ImageBaseUrl + Constants.DefaultImageUrl)
                                    .placeholder(R.drawable.profile_pic_bg)
                                    .transform(new RoundedCornersTransform())
                                    .error(R.drawable.profile_pic_bg).into(img_profilePic);
                        }
                    }
                }
            }
        }
    }
}