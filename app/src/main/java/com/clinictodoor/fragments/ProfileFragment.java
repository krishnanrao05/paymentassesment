package com.clinictodoor.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateProfileActivity;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.ProfileDto;
import com.clinictodoor.utils.BlurBuilder;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.RoundedCornersTransform;
import com.clinictodoor.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements ResponseListner, View.OnClickListener {

    Context ctx;
    ImageView img_profilePic, img_updatePic, img_editProfile, img_back, img_blur;
    TextView tv_name, tv_mobile, tv_dob, tv_gender, tv_emrNo;
    String mobile_num;
    TextView tv_myFamily, myFamily, tv_myAppointments, myAppointments, tv_myAddresses, myAddresses;
    ProfileDto profile_dto;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String s) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        ctx = this.getActivity();
        ((HomeActivity) ctx).myProfileBtnClick();

        img_profilePic = rootView.findViewById(R.id.img_profilePic);
        tv_name = rootView.findViewById(R.id.tv_name);
        tv_mobile = rootView.findViewById(R.id.tv_mobile);
        tv_dob = rootView.findViewById(R.id.tv_dob);
        tv_gender = rootView.findViewById(R.id.tv_gender);
        tv_myFamily = rootView.findViewById(R.id.tv_myFamily);
        myFamily = rootView.findViewById(R.id.myFamily);
        tv_myAppointments = rootView.findViewById(R.id.tv_myAppointments);
        myAppointments = rootView.findViewById(R.id.myAppointments);
        tv_myAddresses = rootView.findViewById(R.id.tv_myAddresses);
        myAddresses = rootView.findViewById(R.id.myAddresses);
        img_updatePic = rootView.findViewById(R.id.img_updatePic);
        img_editProfile = rootView.findViewById(R.id.img_editProfile);
        img_back = rootView.findViewById(R.id.img_back);
        img_blur = rootView.findViewById(R.id.img_blur);
        tv_emrNo = rootView.findViewById(R.id.tv_emrNo);

        tv_name.setTypeface(Utils.mTypeface(ctx, 4));
        tv_mobile.setTypeface(Utils.mTypeface(ctx, 4));
        tv_dob.setTypeface(Utils.mTypeface(ctx, 4));
        tv_gender.setTypeface(Utils.mTypeface(ctx, 4));
        tv_myFamily.setTypeface(Utils.mTypeface(ctx, 4));
        myFamily.setTypeface(Utils.mTypeface(ctx, 3));
        tv_myAppointments.setTypeface(Utils.mTypeface(ctx, 4));
        myAppointments.setTypeface(Utils.mTypeface(ctx, 3));
        tv_myAddresses.setTypeface(Utils.mTypeface(ctx, 4));
        myAddresses.setTypeface(Utils.mTypeface(ctx, 3));
        tv_emrNo.setTypeface(Utils.mTypeface(ctx, 4));
        img_updatePic.setOnClickListener(this);
        img_editProfile.setOnClickListener(this);
        img_back.setOnClickListener(this);

        Bitmap resultBmp = BlurBuilder.blur(ctx, BitmapFactory.decodeResource(getResources(), R.mipmap.app_logo_icon));
        img_blur.setImageBitmap(resultBmp);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("login_access", Context.MODE_PRIVATE);
        mobile_num = sharedPrefgcm.getString("mobile", "");

//        try {
//            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetAddressUrl), ProfileFragment.this, false);
//            as_profile.execute("");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            AsynTask as_profile = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.SaveProfileUrl), ProfileFragment.this, false);
            as_profile.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.SaveProfileUrl)) {
            profile_dto = jsonParser.mUserProfile(response);
            if (profile_dto.getLast_name() != null) {
                tv_name.setText(profile_dto.getSalutation() + " " + profile_dto.getFirst_name() + " " + profile_dto.getLast_name());
            } else {
                tv_name.setText("Create profile");
            }
            tv_mobile.setText(mobile_num);
            tv_emrNo.setText("EMR NO. " + profile_dto.getEmr_no());
            if (profile_dto.getDob() != null) {
                String[] dob = profile_dto.getDob().split("-");
                String age = Utils.getAge(Integer.parseInt(dob[0]), Integer.parseInt(dob[1]), Integer.parseInt(dob[2]));
                tv_dob.setText(age);
            } else {
                tv_dob.setText("--");
            }
            if (profile_dto.getGender() != null) {
                tv_gender.setText(profile_dto.getGender());
            } else {
                tv_gender.setText("----");
            }
//
//            if (profile_dto.getEmail().equals("null")) {
//                tv_email.setText("No email provided.");
//            } else {
//                tv_email.setText(profile_dto.getEmail());
//            }

            Picasso.with(ctx).load(Constants.ImageBaseUrl + profile_dto.getPic())
                    .placeholder(R.drawable.profile_pic_bg)
                    .transform(new RoundedCornersTransform())
                    .error(R.drawable.profile_pic_bg).into(img_profilePic);

            JSONObject jobj = new JSONObject(response);
            if (jobj.has("user_profile")) {
                JSONObject json = jobj.getJSONObject("user_profile");
                if (json.has("addresses")) {
                    JSONArray jarr_address = json.getJSONArray("addresses");
                    tv_myAddresses.setText("" + jarr_address.length());
                }
                if (json.has("family_members")) {
                    JSONArray jarr_family = json.getJSONArray("family_members");
                    tv_myFamily.setText("" + jarr_family.length());
                }
            }


        } /*else if (path.contains(Constants.GetAddressUrl)) {
            List<AddressDto> list_address = jsonParser.mAddressList(response);
            if (list_address.size() != 0) {
                AddressDto address_dto = list_address.get(0);
                tv_address.setVisibility(View.VISIBLE);
                addressType.setVisibility(View.VISIBLE);
                view_line.setVisibility(View.VISIBLE);
                tv_viewMore.setVisibility(View.VISIBLE);
                addressType.setText(address_dto.getAddress_type());
                tv_address.setText(address_dto.getDoor_no() + "," + address_dto.getStreet() + "\n" + address_dto.getLandmark() + "," + address_dto.getCity() + "\n" + address_dto.getState() + "," + address_dto.getCountry() + "-" + address_dto.getPin_code());
            } else {
                view_line.setVisibility(View.GONE);
                tv_viewMore.setVisibility(View.GONE);
                tv_address.setVisibility(View.GONE);
                addressType.setVisibility(View.GONE);
            }
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                getFragmentManager().popBackStack();
                break;
            case R.id.img_editProfile:
                Intent in_profile = new Intent(ctx, CreateProfileActivity.class);
                in_profile.putExtra("profile", profile_dto);
                startActivity(in_profile);
                break;
            case R.id.img_updatePic:
                if (!Utils.checkPermission(ctx)) {
                    Utils.requestPermission((HomeActivity) ctx);
                } else if (profile_dto == null) {
                    Utils.customToast(ctx, "Please create your profile first.");
                } else {
                    Fragment fragment = UpdateProfileFragment.newInstance("");
                    FragmentTransaction fm_update = getFragmentManager().beginTransaction();
                    fm_update.addToBackStack("1").commit();
                    fm_update.replace(R.id.content_frame, fragment, null);
                }
                break;
        }
    }
}
