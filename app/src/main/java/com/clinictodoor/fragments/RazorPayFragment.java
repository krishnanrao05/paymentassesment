package com.clinictodoor.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.clinictodoor.R;

import org.json.JSONArray;
import org.json.JSONException;


/**
 * A simple {@link Fragment} subclass.
 */
public class RazorPayFragment extends Fragment {

     WebView rayzorWebView;
     String cartData;
    JSONArray jsonArray;
    ProgressDialog progressBar;
    public RazorPayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            cartData = getArguments().getString("cartData");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_razor_pay, container, false);
        rayzorWebView = (WebView) view.findViewById(R.id.rayzorWebView);
        WebSettings webSettings = rayzorWebView.getSettings();
        try {
            jsonArray = new JSONArray(cartData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressBar = progressDialog;
        progressBar.show();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        rayzorWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished (WebView view, String url) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    view.evaluateJavascript("$('"+jsonArray+"')", null);
                    view.evaluateJavascript("cartData('"+jsonArray+"')", null);
                    progressBar.dismiss();
                } else {
                    view.loadUrl("javascript:$('"+jsonArray+"')");
                    view.loadUrl("javascript:cartData('"+jsonArray+"')");
                }
            }
        });
        rayzorWebView.loadUrl("file:///android_asset/customWebView.html");
        return view;
    }

}
