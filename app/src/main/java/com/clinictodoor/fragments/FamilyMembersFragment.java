package com.clinictodoor.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.CreateFamilyMemberActivity;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.FamilyMembersAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;

import org.json.JSONException;

import java.net.URL;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamilyMembersFragment extends Fragment implements ResponseListner, View.OnClickListener {

    Context ctx;
    RecyclerView rec_familyMembers;
    FamilyMembersAdapter familyMembersAdapter;
    TextView tv_emptyMembers;
    FloatingActionButton fab_add;

    public FamilyMembersFragment() {
        // Required empty public constructor
    }

    public static FamilyMembersFragment newInstance(String s) {
        FamilyMembersFragment fragment = new FamilyMembersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_family_members, container, false);
        ctx = this.getActivity();
        ((HomeActivity) ctx).myFamilyBtnClick();
        fab_add = rootView.findViewById(R.id.fab_add);
        tv_emptyMembers = rootView.findViewById(R.id.tv_emptyMembers);
        rec_familyMembers = rootView.findViewById(R.id.rec_familyMembers);
        rec_familyMembers.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));


        tv_emptyMembers.setTypeface(Utils.mTypeface(ctx, 3));

        fab_add.setOnClickListener(this);

        rec_familyMembers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab_add.getVisibility() == View.VISIBLE) {
                    fab_add.hide();
                } else if (dy < 0 && fab_add.getVisibility() != View.VISIBLE) {
                    fab_add.show();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            AsynTask as_family = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetFamilyMembersUrl), FamilyMembersFragment.this, false);
            as_family.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        JsonParser jsonParser = new JsonParser();
        if (path.contains(Constants.GetFamilyMembersUrl)) {
            List<FamilyMembersDto> list = jsonParser.mFamilyMembList(response);
            if (list.size() != 0) {
                tv_emptyMembers.setVisibility(View.GONE);
                rec_familyMembers.setVisibility(View.VISIBLE);
                familyMembersAdapter = new FamilyMembersAdapter(ctx, list);
                rec_familyMembers.setAdapter(familyMembersAdapter);
            } else {
                tv_emptyMembers.setVisibility(View.VISIBLE);
                rec_familyMembers.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                Intent intent = new Intent(ctx, CreateFamilyMemberActivity.class);
                startActivity(intent);
                break;
        }
    }
}
