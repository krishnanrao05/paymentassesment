package com.clinictodoor.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.adapters.PackagesAdapter;
import com.clinictodoor.adapters.TestsDialogAdapter;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.PackagesDto;
import com.clinictodoor.dtos.ServicesDto;
import com.clinictodoor.utils.ItemClickSupport;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class PackagesDialog extends Dialog implements View.OnClickListener {

    Context ctx;
    RecyclerView lst_packages;
    TextView tv_title;
    ImageView img_cancel;
    List<PackagesDto> list_packages;
    TestsDialogAdapter testsDialogAdapter;
    List<AddressDto> list_addresses;

    public PackagesDialog(Context ctx, List<PackagesDto> list_packages, List<AddressDto> list_addresses) {
        super(ctx);
        this.ctx = ctx;
        this.list_packages = list_packages;
        this.list_addresses = list_addresses;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.packages_dialog);

        lst_packages = findViewById(R.id.lst_packages);
        lst_packages.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        tv_title = findViewById(R.id.tv_title);
        img_cancel = findViewById(R.id.img_cancel);

        tv_title.setTypeface(Utils.mTypeface(ctx, 4));


        img_cancel.setOnClickListener(this);

        testsDialogAdapter = new TestsDialogAdapter(ctx, list_packages);
        lst_packages.setAdapter(testsDialogAdapter);

        ItemClickSupport.addTo(lst_packages)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        CartDto dto = new CartDto();
                        dto.setService_id(list_packages.get(position).getService_id());
                        dto.setService_name(list_packages.get(position).getTitle());
                        dto.setTest_id(list_packages.get(position).getId());
                        dto.setPackage_type("null");
                        dto.setService_image(list_packages.get(position).getPrice());
                        new AddToCartDialog(ctx, dto, list_addresses).show();
                        dismiss();
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_cancel:
                dismiss();
                break;
        }
    }
}
