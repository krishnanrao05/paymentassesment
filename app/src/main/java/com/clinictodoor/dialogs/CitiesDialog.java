package com.clinictodoor.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.NoGpsActivity;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.dtos.CitiesDto;
import com.clinictodoor.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CitiesDialog extends Dialog implements ResponseListner, View.OnClickListener {

    Context context;
    ListView lst_cities;
    TextView tv_title;
    Button btnCancel;
    List<CitiesDto> list_cities;
    String from;

    public CitiesDialog(Context context, String from) {
        super(context);
        this.context = context;
        this.from = from;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.cities_dialog);


        lst_cities = findViewById(R.id.lst_cities);

        tv_title = findViewById(R.id.tv_title);
        btnCancel = findViewById(R.id.btnCancel);

        try {
            AsynTask as_services = new AsynTask(context, new URL(Constants.BaseUrl + Constants.GetCitiesUrl), CitiesDialog.this, false);
            as_services.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnCancel.setOnClickListener(this);

        lst_cities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setAction("sendBroadCast");
                intent.putExtra("city", list_cities.get(i).getCity());
                context.sendBroadcast(intent);
                if (from.equals("gps")) {
                    ((NoGpsActivity) context).finish();
                }
                dismiss();
            }
        });

    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.GetCitiesUrl)) {
            JSONObject jobj = new JSONObject(response);
            list_cities = new ArrayList<>();
            if (jobj.has("cities")) {
                JSONArray jarr = jobj.getJSONArray("cities");
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject json = jarr.getJSONObject(i);
                    list_cities.add(mCitiesDetails(json));
                }
                List<String> list = new ArrayList<>();
                for (int i = 0; i < list_cities.size(); i++) {
                    list.add(list_cities.get(i).getCity());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, list);
                lst_cities.setAdapter(adapter);
            }
        }
    }

    private CitiesDto mCitiesDetails(JSONObject json) throws JSONException {
        CitiesDto dto = new CitiesDto();
        if (json.has("id")) {
            dto.setId(json.getString("id"));
        }
        if (json.has("city")) {
            dto.setCity(json.getString("city"));
        }
        return dto;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }
}
