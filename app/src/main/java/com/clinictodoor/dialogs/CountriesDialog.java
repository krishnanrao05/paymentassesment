package com.clinictodoor.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.SignUpActivity;
import com.clinictodoor.adapters.CountriesAdapter;
import com.clinictodoor.dtos.CountriesDto;
import com.clinictodoor.utils.ItemClickSupport;
import com.clinictodoor.utils.Utils;

import java.util.List;

public class CountriesDialog extends Dialog {

    Context context;
    RecyclerView rec_countries;
    List<CountriesDto> list_country;
    CountriesAdapter countriesAdapter;
    TextView tv_title, tv_cancel, tv_country;

    public CountriesDialog(@NonNull Context context, List<CountriesDto> list_country, TextView tv_country) {
        super(context);
        this.context = context;
        this.list_country = list_country;
        this.tv_country = tv_country;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.countries_dialog);

        tv_title = findViewById(R.id.tv_title);
        tv_cancel = findViewById(R.id.tv_cancel);
        rec_countries = findViewById(R.id.rec_countries);
        rec_countries.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        countriesAdapter = new CountriesAdapter(context, list_country);
        rec_countries.setAdapter(countriesAdapter);


        tv_title.setTypeface(Utils.mTypeface(context, 4));
        tv_cancel.setTypeface(Utils.mTypeface(context, 4));

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        ItemClickSupport.addTo(rec_countries)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        tv_country.setText(list_country.get(position).getName());
                        ((SignUpActivity) context).gettingCodes(list_country.get(position).getCountry_code(), list_country.get(position).getCalling_code());
                        dismiss();
                    }
                });

    }
}
