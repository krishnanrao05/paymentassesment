package com.clinictodoor.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.clinictodoor.R;
import com.clinictodoor.activities.HomeActivity;
import com.clinictodoor.adapters.AddressesSpinnerAdapter;
import com.clinictodoor.adapters.FamilySpinnerAdapter;
import com.clinictodoor.asynctask.AsynTask;
import com.clinictodoor.asynctask.ResponseListner;
import com.clinictodoor.database.DBHelper;
import com.clinictodoor.dtos.AddressDto;
import com.clinictodoor.dtos.CartDto;
import com.clinictodoor.dtos.FamilyMembersDto;
import com.clinictodoor.utils.Constants;
import com.clinictodoor.utils.JsonParser;
import com.clinictodoor.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddToCartDialog extends Dialog implements View.OnClickListener, DatePickerDialog.OnDateSetListener, ResponseListner {

    Context ctx;
    RelativeLayout rel_date;
    String date = "";
    TextView tv_date, tv_cancle, tv_addToCart;
    LinearLayout ll_time;
    Spinner spn_timeSlots, spn_Family, spn_addresses;
    DBHelper db;
    CartDto dto_cart;
    AddressesSpinnerAdapter addressesSpinnerAdapter;
    FamilySpinnerAdapter familySpinnerAdapter;
    List<AddressDto> list_addresses;
    List<FamilyMembersDto> list_family = new ArrayList<>();

    public AddToCartDialog(@NonNull Context ctx, CartDto dto_cart, List<AddressDto> list_addresses) {
        super(ctx);
        this.ctx = ctx;
        this.dto_cart = dto_cart;
        this.list_addresses = list_addresses;
        db = new DBHelper(ctx);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.addto_cart_dialog);

        rel_date = findViewById(R.id.rel_date);
        tv_date = findViewById(R.id.tv_date);
        ll_time = findViewById(R.id.ll_time);
        spn_timeSlots = findViewById(R.id.spn_timeSlots);
        spn_Family = findViewById(R.id.spn_Family);
        spn_addresses = findViewById(R.id.spn_addresses);
        tv_cancle = findViewById(R.id.tv_cancle);
        tv_addToCart = findViewById(R.id.tv_addToCart);


        rel_date.setOnClickListener(this);
        tv_cancle.setOnClickListener(this);
        tv_addToCart.setOnClickListener(this);

        tv_addToCart.setTypeface(Utils.mTypeface(ctx, 4));
        tv_cancle.setTypeface(Utils.mTypeface(ctx, 3));

        addressesSpinnerAdapter = new AddressesSpinnerAdapter(ctx, 0, list_addresses);
        spn_addresses.setAdapter(addressesSpinnerAdapter);

        try {
            AsynTask as_family = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.GetFamilyMembersUrl), AddToCartDialog.this, false);
            as_family.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rel_date:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        AddToCartDialog.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMinDate(now);
                dpd.show(((HomeActivity) ctx).getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.tv_cancle:
                dismiss();
                break;
            case R.id.tv_addToCart:
                String date = tv_date.getText().toString().trim();
                if (date.equals("DD-MM-YYYY")) {
                    Utils.customToast(ctx, "Please Select Service Date");
                } else {
                    dto_cart.setService_date(date);
                    dto_cart.setService_time(spn_timeSlots.getSelectedItem().toString());
                    dto_cart.setService_family_id(list_family.get(spn_Family.getSelectedItemPosition()).getId());
                    dto_cart.setFamily_name(list_family.get(spn_Family.getSelectedItemPosition()).getRelationship());
                    dto_cart.setService_address_id(list_addresses.get(spn_addresses.getSelectedItemPosition()).getId());
                    dto_cart.setAddress_name(list_addresses.get(spn_addresses.getSelectedItemPosition()).getAddress_type());
                    if (list_family.get(spn_Family.getSelectedItemPosition()).getRelationship().equals("Myself")) {
                        dto_cart.setCart_type("1");
                    } else {
                        dto_cart.setCart_type("0");
                    }
                    long value = db.mInsertCartDetails(dto_cart);
                    if (value == -1) {
                        Utils.customToast(ctx, "Failed to Add to Cart");
                    } else {
                        Utils.customToast(ctx, "Added to Cart Successfully");
                        List<CartDto> list_cart = db.getCartList();
                        ((HomeActivity) ctx).cartCount("" + list_cart.size());
                    }
                }
                dismiss();
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = "";
        String month = "", day = "";
        int mnth = ++monthOfYear;
        if (mnth < 10) {
            month = "0" + mnth;
        } else {
            month = "" + mnth;
        }
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }
        date = year + "-" + month + "-" + day;
        tv_date.setText(day + "-" + month + "-" + year);

        try {
            JSONObject jobj_checkSlots = new JSONObject();
            jobj_checkSlots.put("date", date);
            jobj_checkSlots.put("service_id", dto_cart.getService_id());
            AsynTask as_checkSlots = new AsynTask(ctx, new URL(Constants.BaseUrl + Constants.AvailableSlotsUrl), AddToCartDialog.this, true);
            as_checkSlots.execute("" + jobj_checkSlots);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void serverResponse(String response, String path) throws JSONException, Exception {
        if (path.contains(Constants.AvailableSlotsUrl)) {
            JSONObject jobj = new JSONObject(response);
            if (jobj.has("available_slots")) {
                List<String> list_slots = new ArrayList<>();
                JSONArray jarr = jobj.getJSONArray("available_slots");
                for (int i = 0; i < jarr.length(); i++) {
                    list_slots.add(jarr.getString(i));
                }
                if (list_slots.size() != 0) {
                    ll_time.setVisibility(View.VISIBLE);
                    ArrayAdapter<String> adapter_slots = new ArrayAdapter<String>(ctx, R.layout.spinner_item, list_slots);
                    adapter_slots.setDropDownViewResource(android.R.layout.simple_list_item_1);
                    spn_timeSlots.setAdapter(adapter_slots);
                } else {
                    ll_time.setVisibility(View.GONE);
                    Utils.customToast(ctx, "No Slots Available On This Date");
                }
            }
        } else if (path.contains(Constants.GetFamilyMembersUrl)) {
            JsonParser jsonParser = new JsonParser();
            list_family.clear();
            list_family = jsonParser.mFamilyMembList(response);
            FamilyMembersDto membersDto = new FamilyMembersDto();
            membersDto.setId(dto_cart.getService_family_id());
            membersDto.setRelationship("Myself");
            list_family.add(membersDto);

            familySpinnerAdapter = new FamilySpinnerAdapter(ctx, 0, list_family);
            spn_Family.setAdapter(familySpinnerAdapter);

        }
    }
}
