package com.clinictodoor.asynctask;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHelper {
    static int connectionTimeout = 100000;
    static int readTimeout = 100000;

    public static String postRequestToServer(String body, URL url, Context ctx) {


        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("login_access", Context.MODE_PRIVATE);
        String access_token = sharedPrefgcm.getString("access_token", "");

        String res = null;
        try {
//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost(url.toString());
////		    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
////		      nameValuePairs.add(new BasicNameValuePair("data", data[0]));
//            StringEntity entity = new StringEntity(body, HTTP.UTF_8);
//            entity.setContentType("application/x-www-form-urlencoded");
//            httppost.setEntity(entity);
//
//            //execute http post
//            HttpResponse response = httpclient.execute(httppost);
//
//            InputStream is = response.getEntity().getContent();
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            if (!access_token.equals(""))
                connection.setRequestProperty("Authorization", "Bearer " + access_token);
            connection.setConnectTimeout(connectionTimeout);
            connection.setReadTimeout(readTimeout);
            connection.setDoOutput(true);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
            outputStreamWriter.write(body);
            outputStreamWriter.flush();
            outputStreamWriter.close();

            int respCode = connection.getResponseCode();
            // String me = con nection.getResponseMessage();
            BufferedReader bufferedReader;
            if (connection.getResponseCode() == 200) {
                bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else {
                bufferedReader = new BufferedReader(new
                        InputStreamReader(connection.getErrorStream()));
            }
            // InputStream inputStream = connection.getInputStream();

            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\r\n");
            }
            res = stringBuilder.toString();

        } catch (IOException e) {
            System.out.println("Exception - " + e);
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Exception - " + e);
            e.printStackTrace();
        }
        return res;
    }

    public static String getRequestfromService(URL url, Context ctx) {

        SharedPreferences sharedPrefgcm = ctx.getSharedPreferences("login_access", Context.MODE_PRIVATE);
        String access_token = sharedPrefgcm.getString("access_token", "");
        String response1 = null;

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            if (!access_token.equals("")) {
                connection.setRequestProperty("Authorization", "Bearer " + access_token);
            }else{
                connection.setRequestProperty("Authorization", "");
            }
            connection.connect();
            connection.getErrorStream();
            BufferedReader bufferedReader;
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\r\n");
            }
            response1 = stringBuilder.toString();

        } catch (IOException e) {
            System.out.println("Exception - " + e);
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Exception - " + e);
            e.printStackTrace();
        }
        return response1;

    }
}
