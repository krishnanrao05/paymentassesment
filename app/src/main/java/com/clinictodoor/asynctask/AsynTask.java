package com.clinictodoor.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.net.URL;

// Function to do Async Tracking
public class AsynTask extends AsyncTask<String, Object, String> {

    private ProgressDialog progressBar;
    private static Context ctx;
    boolean type;
    private boolean show_progress = true;

    private URL url;
    private ResponseListner responseListener;
    private String network;

    public AsynTask(Context ctx, URL url) {
        AsynTask.ctx = ctx;
        this.url = url;

        responseListener = (ResponseListner) ((AppCompatActivity) ctx);
    }

    public AsynTask(Context ctx, URL url, ResponseListner listner, boolean post) {
        AsynTask.ctx = ctx;
        this.url = url;
        responseListener = listner;
        this.type = post;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (show_progress) {
            ProgressDialog progressDialog = new ProgressDialog((AppCompatActivity) ctx);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressBar = progressDialog;
            progressBar.show();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        if (hasConnection()) {
            if (type) {
                response = HttpHelper.postRequestToServer(params[0], url, ctx);
            } else {
                response = HttpHelper.getRequestfromService(url, ctx);
            }
        } else {
            network = "No Connection";
            System.out.println("post request - " + response);
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (show_progress)
            dismissProgressDialog();
        if (network != null) {
            noConnectionAlert();
            return;
        }

        if (result == null || result.isEmpty()) {
            nullResponseAlert();
        } else {
            try {
                responseListener.serverResponse(result, url.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) ctx.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

    private void noConnectionAlert() {
        Toast.makeText(ctx, "No Internet Connection.", Toast.LENGTH_SHORT).show();

    }

    private void nullResponseAlert() {

//        Constants.generatetoast(ctx, R.drawable.failed,
//                "Something went wrong please try after some time or login again");
        // Finish_Dialogs f_d = new Finish_Dialogs(ctx, 0, Constants.App_name,
        // "");
        // f_d.show();
    }

    private void dismissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }

    public void setShow_progress(boolean show_progress) {
        this.show_progress = show_progress;
    }
}
