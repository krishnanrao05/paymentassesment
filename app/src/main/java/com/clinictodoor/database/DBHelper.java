package com.clinictodoor.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.clinictodoor.dtos.CartDto;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "ClinicToDoor";


    private static final String TABLE_CART = "table_cart";

    private static final String Cart_Table_ID = "cart_table_id";
    private static final String CartServiceID = "cart_service_id";
    private static final String CartServiceName = "cart_service_name";
    private static final String CartServiceImage = "cart_service_image";
    private static final String CartItemServiceDate = "cart_service_date";
    private static final String CartItemServiceTime = "cart_service_tine";
    private static final String CartServiceFamilyID = "cart_family_id";
    private static final String CartServiceAddressID = "cart_address_id";
    private static final String Cart_TypeID = "cart_type_id";
    private static final String FamilyName = "family_name";
    private static final String AddressName = "address_name";
    private static final String PackageType = "package_type";
    private static final String Test_ID = "test_id";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CART + "("
                + Cart_Table_ID + " INTEGER PRIMARY KEY," + CartServiceID + " TEXT," + CartServiceName + " TEXT," + CartServiceImage + " TEXT,"
                + FamilyName + " TEXT," + AddressName + " TEXT," + PackageType + " TEXT," + Test_ID + " TEXT," + CartItemServiceDate + " TEXT,"
                + CartItemServiceTime + " TEXT," + CartServiceFamilyID + " TEXT," + Cart_TypeID + " TEXT," + CartServiceAddressID + " TEXT"
                + ")";

        db.execSQL(CREATE_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
    }


    public long mInsertCartDetails(CartDto dto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CartServiceID, dto.getService_id());
        values.put(CartServiceName, dto.getService_name());
        values.put(CartServiceImage, dto.getService_image());
        values.put(CartItemServiceDate, dto.getService_date());
        values.put(CartItemServiceTime, dto.getService_time());
        values.put(CartServiceFamilyID, dto.getService_family_id());
        values.put(CartServiceAddressID, dto.getService_address_id());
        values.put(Cart_TypeID, dto.getCart_type());
        values.put(FamilyName, dto.getFamily_name());
        values.put(AddressName, dto.getAddress_name());
        values.put(PackageType, dto.getPackage_type());
        values.put(Test_ID, dto.getTest_id());

        long value = db.insert(TABLE_CART, null, values);
        db.close();
        return value;
    }

    public List<CartDto>    getCartList() {
        List<CartDto> list_dto = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectString = "SELECT * FROM " + TABLE_CART;
        Cursor cursor = db.rawQuery(selectString, null);
        if (cursor.moveToFirst()) {
            do {
                CartDto dto = new CartDto();
                dto.setId(cursor.getString(cursor.getColumnIndex(Cart_Table_ID)));
                dto.setService_id(cursor.getString(cursor.getColumnIndex(CartServiceID)));
                dto.setService_name(cursor.getString(cursor.getColumnIndex(CartServiceName)));
                dto.setService_image(cursor.getString(cursor.getColumnIndex(CartServiceImage)));
                dto.setService_date(cursor.getString(cursor.getColumnIndex(CartItemServiceDate)));
                dto.setService_time(cursor.getString(cursor.getColumnIndex(CartItemServiceTime)));
                dto.setService_family_id(cursor.getString(cursor.getColumnIndex(CartServiceFamilyID)));
                dto.setService_address_id(cursor.getString(cursor.getColumnIndex(CartServiceAddressID)));
                dto.setCart_type(cursor.getString(cursor.getColumnIndex(Cart_TypeID)));
                dto.setFamily_name(cursor.getString(cursor.getColumnIndex(FamilyName)));
                dto.setAddress_name(cursor.getString(cursor.getColumnIndex(AddressName)));
                dto.setPackage_type(cursor.getString(cursor.getColumnIndex(PackageType)));
                dto.setTest_id(cursor.getString(cursor.getColumnIndex(Test_ID)));
                list_dto.add(dto);
            } while (cursor.moveToNext());
        }
        return list_dto;
    }

    public long deleteCartItem(String cart_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long value = db.delete(TABLE_CART, Cart_Table_ID + " = ?",
                new String[]{String.valueOf(cart_id)});
        db.close();
        return value;
    }
}
