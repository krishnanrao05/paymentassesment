package com.clinictodoor.dtos;

import java.io.Serializable;

/**
 * Created by lenovo on 6/2/2018.
 */

public class UserDetailsDto implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccount_verified() {
        return account_verified;
    }

    public void setAccount_verified(String account_verified) {
        this.account_verified = account_verified;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    private String id, username, account_verified, user_type, created_at, updated_at;

    public ProfileDto getProfileDto() {
        return profileDto;
    }

    public void setProfileDto(ProfileDto profileDto) {
        this.profileDto = profileDto;
    }

    private ProfileDto profileDto;
}
