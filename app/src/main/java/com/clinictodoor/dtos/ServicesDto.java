package com.clinictodoor.dtos;

import java.io.Serializable;

/**
 * Created by lenovo on 6/10/2018.
 */

public class ServicesDto implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHas_fixed_price() {
        return has_fixed_price;
    }

    public void setHas_fixed_price(String has_fixed_price) {
        this.has_fixed_price = has_fixed_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private String id;
    private String title;
    private String code;
    private String has_fixed_price;
    private String price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public String getHas_multiple_tests() {
        return has_multiple_tests;
    }

    public void setHas_multiple_tests(String has_multiple_tests) {
        this.has_multiple_tests = has_multiple_tests;
    }

    public String getHas_multiple_packages() {
        return has_multiple_packages;
    }

    public void setHas_multiple_packages(String has_multiple_packages) {
        this.has_multiple_packages = has_multiple_packages;
    }

    private String has_multiple_tests;
    private String has_multiple_packages;
    private Integer icon;
}
